Repository: https://gitlab.upt.ro/raluca.aron/memories

Pasii de rulare a aplicatiei:

1. Instalare nodejs: se acceseaza https://nodejs.org/en, de unde se descarca versiunea care are Long-term support (lts), apoi pentru descarcare se apasa dublu click pe fisierul descarcat, se urmeaza pasii din installer

2. Verifica daca nodejs-ul este instalat corespunzator:
```bash
node --version
npm --version
```

3. Daca nodejs este instalat corespunzator incepem instalarea managerului de pachete yarn si rulam comanda:
```bash
npm install --global yarn
```

4. Verificam daca am instalat corect yarn prin rularea comenzii:
```bash
yarn --version
```

5. Daca nodejs este instalat corespunzator se ruleaza urmatoarele comenzi:

pentru a instala toate dependintele din package.json de care avem nevoie pentru a rula proiectul:
```bash
yarn install
```

pentru pornirea aplicatiei rulam comanda:
```bash
yarn run start
```