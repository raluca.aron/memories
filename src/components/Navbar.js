
import React, { useContext, useRef } from 'react'; 
import { Menubar } from 'primereact/menubar';
import { Menu } from 'primereact/menu';
import { Avatar } from 'primereact/avatar';
import { getAuth, signOut } from 'firebase/auth';
import { RoutingContext } from '../contexts/RoutingContext';
import { UserContext } from '../contexts/UserContext';
import { serverTimestamp } from '@firebase/firestore';
import { setUserData } from '../services/UserOperations';
import { useWindowSize } from '../constants/Functions';
import LogoAppWhite from "../images/logo-app-white.png";

export default function Navbar(props) {
    const {
        labelAvatar
    } = props;
    const menu = useRef(null);
    const [width, height] = useWindowSize();
    const { history } = useContext(RoutingContext);
    const auth = getAuth();
    const {userData: {user}} = useContext(UserContext);
    const subMenuItem = user?.type === "buyer" ? [
        { label: 'Profile', icon: 'pi pi-fw pi-user', command: () => history('/profile') },
        { label: 'Orders', icon: 'pi pi-fw pi-money-bill', 
        command: () => history(`/orders/${user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()}`) },
        { label: 'Favorites', icon: 'pi pi-fw pi-heart-fill', 
        command: () => history(`/favorites/${user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()}`) },
        { label: 'Logout', icon: 'pi pi-fw pi-lock-open', command: async () => {
            if (user) {
                await setUserData("users", user?.email, {
                    ...user,
                    isLogged: false,
                    lastLogged: serverTimestamp()
                }).then(async res => {
                    if (res) {
                        await signOut(auth)
                        .then(() => {
                            // Sign-out successful.
                            history('/');
                        }).catch((error) => {
                            // An error happened.
                        });
                       
                    } else {
                    }
                })
            }
        }},
    ] : user?.type === "admin" ? [
        { label: 'Profile', icon: 'pi pi-fw pi-user', command: () => history('/profile') },
        { label: 'Orders', icon: 'pi pi-fw pi-money-bill', 
        command: () => history(`/orders`) },
        { label: 'Logout', icon: 'pi pi-fw pi-lock-open', command: async () => {
            if (user) {
                await setUserData("users", user?.email, {
                    ...user,
                    isLogged: false,
                    lastLogged: serverTimestamp()
                }).then(async res => {
                    if (res) {
                        await signOut(auth)
                        .then(() => {
                            // Sign-out successful.
                            history('/');
                        }).catch((error) => {
                            // An error happened.
                        });
                       
                    } else {
                    }
                })
            }
        }},
    ] :[
        { label: 'Profile', icon: 'pi pi-fw pi-user', command: () => history('/profile') },
        { label: 'Logout', icon: 'pi pi-fw pi-lock-open', command: async () => {
            if (user) {
                await setUserData("users", user?.email, {
                    ...user,
                    isLogged: false,
                    lastLogged: serverTimestamp()
                }).then(async res => {
                    if (res) {
                        await signOut(auth)
                        .then(() => {
                            // Sign-out successful.
                            history('/');
                        }).catch((error) => {
                            // An error happened.
                        });
                       
                    } else {
                    }
                })
            }
        }},
    ]
    const items = user?.type === "buyer" ? [
        {
            label: 'Home', icon: 'pi pi-fw pi-home',
            command: () => history('/home')
        },
        {
            label: 'Chats', icon: 'pi pi-fw pi-comments',
            command: () => user && 
                    history(`/chatroom`)
        },
        {
            label: 'Shopping Cart', icon: 'pi pi-fw pi-shopping-cart',
            command: () => user && 
                    history(`/shopping-cart/${user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()}`)
        }
    ] : user?.type === "admin" ? [
        {
            label: 'Home', icon: 'pi pi-fw pi-home',
            command: () => history('/home')
        },
        {
            label: 'Users', icon: 'pi pi-fw pi-users',
            command: () => user && 
                    history(`/users`)
        },
        {
            label: 'Categories', icon: 'pi pi-fw pi-tags',
            command: () => user && 
                    history(`/category`)
        }
    ] : [
        {
            label: 'Chats', icon: 'pi pi-fw pi-calendar',
            command: () => user && 
                    history(`/chatroom`)
        },
        {
            label: 'Orders', icon: 'pi pi-fw pi-calendar',
            command: () => user && 
                    history(`/orders/${user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()}`)
        }
    ];

    const smallDeviceItems = user?.type === "buyer" ? [
        {
            label: 'Home', icon: 'pi pi-fw pi-home',
            command: () => history('/home')
        },
        {
            label: 'Chats', icon: 'pi pi-fw pi-comments',
            command: () => user && 
                    history(`/chatroom`)
        },
        {
            label: 'Shopping Cart', icon: 'pi pi-fw pi-shopping-cart',
            command: () => user && 
                    history(`/shopping-cart/${user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()}`)
        },
        { label: 'Profile', icon: 'pi pi-fw pi-user', command: () => history('/profile') },
        { label: 'Orders', icon: 'pi pi-fw pi-money-bill', 
        command: () => history(`/orders/${user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()}`) },
        { label: 'Favorites', icon: 'pi pi-fw pi-heart-fill', 
        command: () => history(`/favorites/${user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()}`) },
        { label: 'Logout', icon: 'pi pi-fw pi-lock-open', command: async () => {
            if (user) {
                await setUserData("users", user?.email, {
                    ...user,
                    isLogged: false,
                    lastLogged: serverTimestamp()
                }).then(async res => {
                    if (res) {
                        await signOut(auth)
                        .then(() => {
                            // Sign-out successful.
                            history('/');
                        }).catch((error) => {
                            // An error happened.
                        });
                       
                    } else {
                    }
                })
            }
        }},
    ] : user?.type === "admin" ? [
        {
            label: 'Home', icon: 'pi pi-fw pi-home',
            command: () => history('/home')
        },
        {
            label: 'Users', icon: 'pi pi-fw pi-users',
            command: () => user && 
                    history(`/users`)
        },
        {
            label: 'Categories', icon: 'pi pi-fw pi-book',
            command: () => user && 
                    history(`/category`)
        },
        { label: 'Profile', icon: 'pi pi-fw pi-user', command: () => history('/profile') },
        { label: 'Orders', icon: 'pi pi-fw pi-money-bill', 
        command: () => history(`/orders`) },
        { label: 'Logout', icon: 'pi pi-fw pi-lock-open', command: async () => {
            if (user) {
                await setUserData("users", user?.email, {
                    ...user,
                    isLogged: false,
                    lastLogged: serverTimestamp()
                }).then(async res => {
                    if (res) {
                        await signOut(auth)
                        .then(() => {
                            // Sign-out successful.
                            history('/');
                        }).catch((error) => {
                            // An error happened.
                        });
                       
                    } else {
                    }
                })
            }
        }},
    ] : [
        {
            label: 'Chats', icon: 'pi pi-fw pi-comments',
            command: () => user && 
                    history(`/chatroom`)
        },
        {
            label: 'Orders', icon: 'pi pi-fw pi-money-bill',
            command: () => user && 
                    history(`/orders/${user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()}`)
        },
        { label: 'Profile', icon: 'pi pi-fw pi-user', command: () => history('/profile') },
        { label: 'Logout', icon: 'pi pi-fw pi-lock-open', command: async () => {
            if (user) {
                await setUserData("users", user?.email, {
                    ...user,
                    isLogged: false,
                    lastLogged: serverTimestamp()
                }).then(async res => {
                    if (res) {
                        await signOut(auth)
                        .then(() => {
                            // Sign-out successful.
                            history('/');
                        }).catch((error) => {
                            // An error happened.
                        });
                       
                    } else {
                    }
                })
            }
        }},
    ];

    const start = <div className='flex align-items-center justify-content-center'><img alt="logo" src={LogoAppWhite} height="30" className="mr-2"></img><h3 className='text-white mr-3'>Memories</h3></div>;
    const end = <>
        <Menu model={subMenuItem} popup ref={menu} />
        <Avatar label={labelAvatar ?? "U"} shape="circle" onClick={(e) => menu.current.toggle(e)} style={{ backgroundColor: "#8cbf7c", color: "#fff" }} />
    </>

    return (
        <div className="card">
            <Menubar model={width < 960 ? smallDeviceItems : items} breakpoint="960px" className='navbar' start={start} end={width > 960 && end} />
        </div>
    )
}
        