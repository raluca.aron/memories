import React from 'react';

export default function CheckoutDetails(props) {
    const {
        data
    } = props;
    
    return (
        <div className="flex flex-column justify-content-center align-items-start w-full p-0 md:p-4 lg:p-4">
            <h2>Order details</h2>
            <div className={"flex flex-column justify-content-center align-items-start "
            + "md:flex-row md:justify-content-between md:align-items-center "
            + "lg:flex-row lg:justify-content-between lg:align-items-center w-full"}>
                <p>Address:</p>
                <p>{data?.address ?? 'No info'}</p>
            </div>
            <div className={"flex flex-column justify-content-center align-items-start "
            + "md:flex-row md:justify-content-between md:align-items-center "
            + "lg:flex-row lg:justify-content-between lg:align-items-center w-full"}>
                <p>Location:</p>
                <p>{data?.location ?? 'No info'}</p>
            </div>
            <div className={"flex flex-column justify-content-center align-items-start "
            + "md:flex-row md:justify-content-between md:align-items-center "
            + "lg:flex-row lg:justify-content-between lg:align-items-center w-full"}>
                <p>Meeting time:</p>
                <p>{data?.calendar ?? 'No info'}</p>
            </div>
        </div>
    )
}