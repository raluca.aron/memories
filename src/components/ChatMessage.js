import { Avatar } from 'primereact/avatar';
import { Button } from 'primereact/button';
import React, { useContext } from 'react';
import { UserContext } from '../contexts/UserContext';
import { setMessage } from '../services/ChatRoom';
import '../styles/ChatMessage.css';

const ChatMessage = (props) => {
    const { userData: { user } } = useContext(UserContext);
    const {
        text,
        isOffer,
        sender
    } = props.message;
    const {
        onOfferAction,
        message,
        photoView
    } = props;
    const setOfferStatus = (status) => {
       if (onOfferAction) {
            onOfferAction(status, message)
       }
    }
    return (
        <div className={`message ${user?.email === sender ? 'sent' : 'received'}`}>
            <Avatar
                label={user?.email === sender ? sender[0]?.toUpperCase() : sender[0]?.toUpperCase()}
                size="large" 
                shape="circle"
                style={{ backgroundColor: user?.email === sender ? "#fff" : "#8cbf7c",
                color: user?.email === sender ? "#8cbf7c" : "#fff",
                border: user?.email === sender ? "1px solid #8cbf7c" : "0"
            }}
            />
            {
                isOffer && text && !photoView ?
                    <div className='offer'>
                        <p>You received the next offer:</p>
                        <p>${JSON.parse(text).price} for {JSON.parse(text).packageName} package</p>
                        <div>
                            {
                                JSON.parse(text).isAccepted ?
                                    <p>Offer Accepted</p>
                                :
                                JSON.parse(text).isAccepted === false ?
                                    <p>Offer Declined</p>
                                :
                                <>
                                    <Button label="Accept" 
                                        severity={JSON.parse(text).isAccepted ? "success" : "secondary"}
                                        disabled={JSON.parse(text).isAccepted}
                                        onClick={() => setOfferStatus(true)}
                                        className='btn-white flex-none border-noround mr-2'
                                    />
                                    <Button label="Decline" 
                                        severity={JSON.parse(text).isAccepted === false ? "danger" : "secondary"}
                                        disabled={JSON.parse(text).isAccepted === false}
                                        onClick={() => setOfferStatus(false)}
                                        className='btn-white flex-none border-noround'
                                    />
                                </>   
                            }
                        </div>
                        
                    </div>
                :
                photoView && isOffer && text ?
                    <div className='offer-photo'>
                        <p>You sent the next offer:</p>
                        <p>${JSON.parse(text).price} for {JSON.parse(text).packageName} package</p>
                        <p>Status: {JSON.parse(text).isAccepted ? "Accepted" : JSON.parse(text).isAccepted === false ? "Declined" : "Need response"}</p>
                    </div>
                :
                    <p>{text}</p>
            }
        </div>
        
    );
};

export default ChatMessage;