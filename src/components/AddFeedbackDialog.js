import React, { useState } from 'react'; 
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { Rating } from 'primereact/rating';
import { InputTextarea } from 'primereact/inputtextarea';

export default function AddFeedbackDialog(props) {
    const [stars, setStars] = useState(0);
    const [reason, setReason] = useState("");

    const {
        visibleDialog,
        setVisibleDialog,
        onSave,
        isOrder
    } = props;

    const handleResetDialog = () => {
        if (setVisibleDialog)
            setVisibleDialog(false);
        setTimeout(() => {
          setStars(0);
        }, 500)
    };

    const handleOnSave = () => {
      if (onSave) {
        if (isOrder) {
          onSave(reason);
        } else
          onSave(stars);
      }
      handleResetDialog();
    }

    const footerContentDialog = (
        <div>
            <Button label="Cancel" icon="pi pi-times" onClick={() => handleResetDialog()} className="btn-white" />
            <Button label="Upload Rating" disabled={(stars === 0 && !isOrder) || (reason === "" && isOrder)} className="btn-fill" icon="pi pi-check" onClick={() => handleOnSave()} autoFocus />
        </div>
    );

    return (
        <Dialog
        header={isOrder ? "Add reason" : "Add rating"}
        visible={visibleDialog} 
        onHide={() => setVisibleDialog ? setVisibleDialog(false) : ''}
        style={{ width: '50vw' }} breakpoints={{ '960px': '75vw', '641px': '100vw' }}
        footer={footerContentDialog}
    >
      {
        isOrder ?
        <div>
          <h3>Reason*</h3>
          <InputTextarea
            className="mt-2"
            value={reason}
            onChange={(e) => setReason(e.target.value)} 
            rows={5} cols={30} 
          />
        </div>
        :
        <div>
          <h3>Rating*</h3>
          <Rating value={stars} onChange={(e) => setStars(e.value)} />
        </div>
      }

    </Dialog>
    )
}