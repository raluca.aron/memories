import React, { useState } from 'react'; 
import DropdownTemplate from './DropdownTemplate';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputNumber } from 'primereact/inputnumber';
import { InputTextarea } from 'primereact/inputtextarea';
import { useWindowSize } from '../constants/Functions';

export default function AddPackageDialog(props) {
    const [namePackage, setNamePackage] = useState('');
    const [descriptionPackage, setDescriptionPackage] = useState('');
    const [categoryPackage, setCategoryPackage] = useState(null);
    const [pricePackage, setPricePackage] = useState(0);
    const [width, height] = useWindowSize();

    const {
        visibleDialog,
        setVisibleDialog,
        categoryOptions,
        onSave,
    } = props;

    const handleResetDialog = () => {
        if (setVisibleDialog)
            setVisibleDialog(false);
        setTimeout(() => {
          setNamePackage('');
          setDescriptionPackage('');
          setCategoryPackage(null);
          setPricePackage(0);
        }, 500)
    };

    const handleOnSave = () => {
      if (onSave) {
        onSave({name: namePackage, description: descriptionPackage, category: categoryPackage.code, price: pricePackage});
      }
      handleResetDialog();
    }

    const footerContentDialog = (
        <div>
            <Button label="Cancel" icon="pi pi-times" onClick={() => handleResetDialog()} className="btn-white mb-4 md:mb-0 lg:mb-0" />
            <Button label="Upload Package" icon="pi pi-check" onClick={() => handleOnSave()} className="btn-fill" autoFocus />
        </div>
    );

    return (
        <Dialog
        header="Add package" 
        visible={visibleDialog} 
        onHide={() => setVisibleDialog ? setVisibleDialog(false) : ''}
        style={{ width: width < 960 ? "60vw" : '50vw' }}
        footer={footerContentDialog}
    >
      <div className='grid'>
        <div className='col-12 md:col-6 lg:col-6'>
          <h3>Package name*</h3>
          <InputText
            value={namePackage}
            onChange={(e) => setNamePackage(e.target.value)}
            className="w-full"
          />
        </div>
        <div className='col-12 md:col-6 lg:col-6'>
          <h3>Description*</h3>
          <InputTextarea
              className="mt-2"
              value={descriptionPackage}
              onChange={(e) => setDescriptionPackage(e.target.value)} 
              rows={5} cols={width < 1430 ? width < 1150 ? 20 : 30 : 40} 
          />
        </div>
        <div className='col-12 md:col-6 lg:col-6'>
          <h3>Category*</h3>
          <DropdownTemplate
              value={categoryPackage} 
              onChange={(e) => setCategoryPackage(e.value)} 
              options={categoryOptions} 
              optionLabel="name" 
              placeholder="Select a option" 
              filter
              showClear
          />
        </div>
        <div className='col-12 md:col-6 lg:col-6'>
          <h3>Price $*</h3>
          <InputNumber value={pricePackage} onValueChange={(e) => setPricePackage(e.value)} className="w-10" />
        </div>
      </div>
    </Dialog>
    )
}