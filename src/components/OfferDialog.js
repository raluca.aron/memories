import React, { useState } from 'react'; 
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputNumber } from 'primereact/inputnumber';
import DropdownTemplate from './DropdownTemplate';


export default function OfferDialog(props) {
    const {
        open,
        categoryOptions,
        onHide,
        onOfferFinished
    } = props;
    const [selectedCategory, setSelectedCategory] = useState(null);
    const [packageName, setPackageName] = useState("");
    const [price, setPrice] = useState(0);

    const onHideDialog = () => {
        setPackageName("");
        setSelectedCategory(null);
        setPrice(0);
        if (onHide)
            onHide();
    }

    return (
        <Dialog header="New offer"
            visible={open} 
            onHide={onHideDialog}
            style={{ width: '50vw' }} breakpoints={{ '960px': '75vw', '641px': '100vw' }}>
            <div className='grid'>
                <div className={"col-12 md:col-6 lg:col-6 flex flex-column justify-content-start align-items-center"}>
                    <h3>Package Name</h3>
                    <InputText
                        value={packageName}
                        onChange={(e) => setPackageName(e.target.value)}
                    />
                </div>
                <div className={"col-12 md:col-6 lg:col-6 flex flex-column justify-content-start align-items-center"}>
                    <h3>Category</h3>
                    <DropdownTemplate
                        value={selectedCategory} 
                        onChange={(e) => setSelectedCategory(e.value)} 
                        options={categoryOptions} 
                        optionLabel="name" 
                        placeholder="Select a option" 
                        filter
                        showClear
                    />
                </div>
                <div className={"col-12 md:col-6 lg:col-6 flex flex-column justify-content-start align-items-center mb-5"}>
                    <h3>Price</h3>
                    <InputNumber
                        inputId="currency-us"
                        value={price}
                        onValueChange={(e) => setPrice(e.value)}
                        mode="currency" 
                        currency="USD"
                        locale="en-US" 
                    />
                </div>
            </div>
            <Button 
            label={"Place offer"}
            disabled={price === 0 || selectedCategory === null || packageName === ""}
            className="btn-fill"
            onClick={() => onOfferFinished ? onOfferFinished({packageName, categoryOptions: selectedCategory, price, isAccepted: null}) : ''} />
        </Dialog>
    )
}