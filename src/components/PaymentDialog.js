import React, { useState } from 'react'; 
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputNumber } from 'primereact/inputnumber';
import { InputMask } from 'primereact/inputmask';


export default function PaymentDialog(props) {
    const {
        open,
        onHide,
        total,
        onPayEvent
    } = props;
    const [cardId, setCardId] = useState(undefined);
    const [cardName, setCardName] = useState(undefined);
    const [cvv, setCVV] = useState(undefined);
    const [date, setDate] = useState(undefined);

    const cardVerification = () => {
        
        if (cardId !== '' && cardName !== '' && cvv !== 0 && date !== '') {


            return true;
        }

        return false;
    }

    const onHideDialog = () => {
        setCardId(undefined);
        setCardName(undefined);
        setCVV(undefined);
        setDate(undefined);
        if (onHide)
            onHide();
    }

    return (
        <Dialog header="Pay online with credit card"
            visible={open} 
            onHide={onHideDialog}
            style={{ width: '50vw' }} breakpoints={{ '960px': '75vw', '641px': '100vw' }}>
             <div className={"flex flex-row justify-content-start align-items-center "}>
                <h3>Total:</h3> <p className='ml-2'>${isNaN(parseInt(total)) ? 0 : total}</p>
            </div>
            <div className={"flex flex-column justify-content-center align-items-start "
                + "md:flex-row md:justify-content-between md:align-items-center "
                + "lg:flex-row lg:justify-content-between lg:align-items-center w-full"}>
                <div className="flex flex-column justify-content-center align-items-start">
                    <h3>Card Owner Number</h3>
                </div>
                <InputMask placeholder="xxxx - xxxx - xxxx - xxxx" mask="9999 - 9999 - 9999 - 9999" value={cardId} onChange={(e) => setCardId(e.target.value)} />
            </div>
            <div className={"flex flex-column justify-content-center align-items-start "
                + "md:flex-row md:justify-content-between md:align-items-center "
                + "lg:flex-row lg:justify-content-between lg:align-items-center w-full"}>
                <div className="flex flex-column justify-content-center align-items-start">
                    <h3>Card Owner Name</h3>
                </div>
                <InputText placeholder="Card Owner Name" value={cardName} onChange={(e) => setCardName(e.target.value)} />
            </div>
            <div className={"flex flex-column justify-content-center align-items-start "
                + "md:flex-row md:justify-content-between md:align-items-center "
                + "lg:flex-row lg:justify-content-between lg:align-items-center w-full"}>
                <div className="flex flex-column justify-content-center align-items-start">
                    <h3>CVV Number</h3>
                    <p>Enter the 3 of 4 digit number on the card</p>
                </div>
                <InputNumber value={cvv} onValueChange={(e) => setCVV(e.value)} placeholder="CVV"/>
            </div>
            <div className={"col-12 md:col-6 lg:col-3 flex flex-column justify-content-center align-items-start "
                + "md:flex-row md:justify-content-between md:align-items-center "
                + "lg:flex-row lg:justify-content-between lg:align-items-center w-full"}>
                <div className="flex flex-column justify-content-center align-items-start">
                    <h3>Expiry Date</h3>
                    <p>Enter the expiration date of the card</p>
                </div>
                <InputMask value={date} onChange={(e) => setDate(e.target.value)} mask="99 / 99" placeholder="MM / YY" />
            </div>
            <Button label={"Pay Now"} className="btn-fill" onClick={() => onPayEvent && cardVerification() ? onPayEvent({cardId: cardId, cardName: cardName, cvv: cvv, date: date}) : ''}/>
        </Dialog>
    )
}