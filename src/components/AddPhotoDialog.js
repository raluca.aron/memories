import React, { useState } from 'react';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';

export default function AddPhotoDialog(props) {
    const [photoDialog, setPhotoDialog] = useState('');
    const {
        visibleDialog,
        setVisibleDialog,
        onUpload,
    } = props;

    const handleResetAddAlbumDialog = () => {
        if (setVisibleDialog)
            setVisibleDialog(false);
        setTimeout(() => {
          setPhotoDialog('');
        }, 500)
    };

    const customBase64Uploader = async (event) => {
        event?.files?.map(async file => {
          const reader = new FileReader();
          let blob = await fetch(file.objectURL).then((r) => r.blob());
      
          reader.readAsDataURL(blob);
      
          reader.onloadend = function () {
              const base64data = reader.result;
              setPhotoDialog(base64data);
          };
        })
    };

    const footerContentDialog = (
        <div>
            <Button label="Cancel" icon="pi pi-times" onClick={() => handleResetAddAlbumDialog()} className="btn-white" />
            <Button label="Upload Photo" icon="pi pi-check" className='btn-fill' onClick={() => onUpload ? onUpload(photoDialog) : ''} autoFocus />
        </div>
    );

    return (
        <Dialog
        header="Upload Photo" 
        visible={visibleDialog} 
        onHide={() => setVisibleDialog ? setVisibleDialog(false) : ''}
        style={{ width: '50vw' }} breakpoints={{ '960px': '75vw', '641px': '100vw' }}
        footer={footerContentDialog}
    >
      <div>
        <FileUpload
          name="demo[]" 
          mode="advanced"
          chooseOptions={{label:"Choose", icon:"pi pi-plus", className:'icon-btn'}}
          auto
          customUpload="true"
          uploadHandler={(e) => customBase64Uploader(e)}
          multiple={false}
          accept="image/*" 
          maxFileSize={1000000} 
          emptyTemplate={<p className="m-0">Drag and drop files to here to upload.</p>} 
        />
      </div>
    </Dialog>
    )
}