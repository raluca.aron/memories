import React from 'react';

export default function PaymentDetails(props) {
    const {
        totalOrder
    } = props;
    
    return (
        <div className="flex flex-column justify-content-center align-items-start w-full p-0 md:p-4 lg:p-4">
            <h2>Order summary</h2>
            <div className={"flex flex-column justify-content-center align-items-start "
            + "md:flex-row md:justify-content-between md:align-items-center "
            + "lg:flex-row lg:justify-content-between lg:align-items-center w-full"}>
                <p>Total Order:</p>
                <p>${isNaN(parseInt(totalOrder)) ? 0 : totalOrder}</p>
            </div>
        </div>
    )
}