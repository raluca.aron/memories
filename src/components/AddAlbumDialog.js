import React, { useContext, useState } from 'react'; 
import { UserContext } from '../contexts/UserContext';
import DropdownTemplate from './DropdownTemplate';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';

export default function AddAlbumDialog(props) {
    const [albumDialogName, setAlbumDialogName] = useState('');
    const [categoryDialogName, setCategoryDialogName] = useState(null);
    const [galleryDialog, setGalleryDialog] = useState([]);
    const {
        visibleDialog,
        setVisibleDialog,
        categoryOptions,
        onSave
    } = props;

    const handleResetAddAlbumDialog = () => {
        if (setVisibleDialog)
            setVisibleDialog(false);
        setTimeout(() => {
          setAlbumDialogName('');
          setCategoryDialogName(null);
          setGalleryDialog([]);
        }, 500)
    };

    const handleUploadAlbumDialog = async () => {
        if (onSave) {
          onSave({category: categoryDialogName?.code, name: albumDialogName, carousel: galleryDialog?.map((element, index) => ({image: element, name: albumDialogName + ' ' + (+index + 1) }))})
        }
        handleResetAddAlbumDialog();
    };
    const customBase64Uploader = async (event) => {

        await Promise.all(event?.files?.map(file => new Promise(async (resolve, reject) => {
          const reader = new FileReader();
          let blob = await fetch(file.objectURL).then((r) => r.blob()); 
      
          reader.readAsDataURL(blob);
      
          reader.onloadend = function () {
              resolve(reader.result);
          };
        }))).then((base64data) => setGalleryDialog(base64data));
    };

    const footerContentDialog = (
        <div>
            <Button label="Cancel" icon="pi pi-times" onClick={() => handleResetAddAlbumDialog()} className="btn-white" />
            <Button label="Upload Album" icon="pi pi-check" onClick={() => handleUploadAlbumDialog()} className="btn-fill" autoFocus />
        </div>
    );

    return (
        <Dialog
        header="Add album" 
        visible={visibleDialog} 
        onHide={() => setVisibleDialog ? setVisibleDialog(false) : ''}
        style={{ width: '50vw' }} breakpoints={{ '960px': '75vw', '641px': '100vw' }}
        footer={footerContentDialog}
    >
       <div className='grid'>
        <div className='col-12 md:col-6 lg:col-6'>
          <h3>Album name*</h3>
          <InputText
            value={albumDialogName}
            onChange={(e) => setAlbumDialogName(e.target.value)}
            className="w-full"
          />
        </div>
        <div className='col-12 md:col-6 lg:col-6'>
          <h3>Category*</h3>
          <DropdownTemplate
              value={categoryDialogName} 
              onChange={(e) => setCategoryDialogName(e.value)} 
              options={categoryOptions} 
              optionLabel="name" 
              placeholder="Select a option" 
              filter
              showClear
          />
        </div>
        <div className='col-12 md:col-6 lg:col-6'>
          <h3>Upload Photos*</h3>
          <FileUpload
            name="demo[]" 
            mode="advanced"
            auto
            customUpload="true"
            uploadHandler={(e) => customBase64Uploader(e)}
            multiple accept="image/*" 
            maxFileSize={1000000} 
            emptyTemplate={<p className="m-0">Drag and drop files to here to upload.</p>} 
          />
        </div>
       </div>
    </Dialog>
    )
}