import React, { useEffect, useState } from 'react'; 
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { Calendar } from 'primereact/calendar';
import { formatDate } from '../constants/Functions';

export default function CheckoutDialog(props) {
    const {
        data,
        open,
        onHide,
        onCompleteAddress
    } = props;
    const [address, setAddress] = useState(data?.address ?? '');
    const [calendar, setCalendar] = useState(data?.calendar ?? null);
    const [location, setLocation] = useState(data?.location ?? '');

    useEffect(() => {
        setAddress(data?.address ?? "");
        setCalendar(data?.calendar ?? null);
        setLocation(data?.location ?? "");
    },[data?.location, open]);

    const onHideDialog = () => {
        setAddress('');
        setCalendar(null);
        if (onHide)
            onHide();
    }

    return (
        <Dialog header="Location Info"
            visible={open} 
            onHide={onHideDialog}
            style={{ width: '50vw' }} breakpoints={{ '960px': '75vw', '641px': '100vw' }}>
            <div className={"flex flex-column justify-content-start align-items-center"}>
                <h3>Address*</h3>
                <InputText placeholder="Address" 
                value={address} onChange={(e) => setAddress(e.target.value)} />
            </div>
            <div className={"flex flex-column justify-content-start align-items-center"}>
                <h3>Location*</h3>
                <InputText placeholder="Location" 
                value={location} onChange={(e) => setLocation(e.target.value)} />
            </div>
            <div className={"flex flex-column justify-content-start align-items-center"}>
                <h3>Meeting time*</h3>
                <Calendar value={calendar} onChange={(e) => setCalendar(e.value)} showTime hourFormat="24" />
            </div>
            <Button label={"Next Step"} className="btn-fill" disabled={address === '' || location === ''} 
            onClick={() => onCompleteAddress ? onCompleteAddress({address: address, location: location, calendar: formatDate(calendar)}) : ''} />
        </Dialog>
    )
}