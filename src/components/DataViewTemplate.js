
import React, { useState, useEffect, useContext } from 'react';
import { DataView, DataViewLayoutOptions } from 'primereact/dataview';
import { Button } from 'primereact/button';
import { Dropdown } from 'primereact/dropdown';
import { Rating } from 'primereact/rating';
import { RoutingContext } from '../contexts/RoutingContext';
import { Galleria } from 'primereact/galleria';

export default function DataViewTemplate(props) {
    const { history } = useContext(RoutingContext);
    const [sortKey, setSortKey] = useState(null);
    const [sortOrder, setSortOrder] = useState(null);
    const [sortField, setSortField] = useState(null);

    const {
        data,
        layout,
        rows,
        setLayout,
        renderListItem,
        renderGridItem,
        sortOptions
    } = props;
    useEffect(() => {
        
    }, []);

    const onSortChange = (event) => {
        const value = event.value;

        if (value.indexOf('!') === 0) {
            setSortOrder(-1);
            setSortField(value.substring(1, value.length));
            setSortKey(value);
        } else {
            setSortOrder(1);
            setSortField(value);
            setSortKey(value);
        }
    };

    const itemListGalleriaImageTemplate = (item) => {
        return <img src={item.src} alt={item.alt} className="md:w-11rem w-9 shadow-2 md:my-0 md:mr-5 mr-0 my-5 w-full h-7rem" />;
    };

    const itemGridGalleriaImageTemplate = (item) => {
        return <img src={item.src} alt={item.alt} className="w-9 my-5 shadow-3 w-full h-15rem" />;
    };

    const renderListItemDefault = (data) => {
        return (
            <div className="col-12">
                <div className="flex flex-column align-items-center p-3 w-full md:flex-row">
                    <Galleria 
                        value={data?.image !== "" ? [{src: data?.image, alt: data?.name}] : []} 
                        showThumbnails={false} 
                        showIndicators
                        style={{ maxWidth: '640px' }} 
                        showIndicatorsOnItem={null} 
                        indicatorsPosition={'bottom'} 
                        item={itemListGalleriaImageTemplate} 
                    />
                    {/* <img className="md:w-11rem w-9 shadow-2 md:my-0 md:mr-5 mr-0 my-5 w-full h-7rem" src={data?.image ?? ''}  alt={data?.name} /> */}
                    <div className="text-center md:text-left md:flex-1">
                        <div className="text-2xl font-bold">{data?.name}</div>
                        <div className="mb-3">{data?.location}</div>
                        <div className="mb-3">{data?.email}</div>
                        <Rating className="mb-2" value={data?.rating} readOnly cancel={false}></Rating>
                        <i className="pi pi-tag vertical-align-middle mr-2"></i>
                        <span className="vertical-align-middle font-semibold">{data?.categories}</span>
                    </div>
                    <div className="flex md:flex-column mt-5 justify-content-between align-items-center md:w-auto w-full">
                        <span className="align-self-center text-2xl font-semibold mb-2 md:align-self-end">${data?.price}</span>
                        <Button className="btn-fill mb-2" icon="pi pi-camera" label="View photographer" onClick={() => history(`/view-photographer/${data?.email}`)}></Button>
                    </div>
                </div>
            </div>
        );
    };

    const renderGridItemDefault = (data) => {
        return (
            <div className="col-12 md:col-4">
                <div className="m-2 border-1 border-round-sm p-2 surface-border card">
                    <div className="flex align-items-center justify-content-between">
                        <div>
                            <i className="pi pi-tag vertical-align-middle mr-2"></i>
                            <span className="vertical-align-middle font-semibold">{data?.categories}</span>
                        </div>
                        <div>
                            <i className="pi pi-tag vertical-align-middle mr-2"></i>
                            <span className="font-semibold vertical-align-middle">{data?.location}</span>
                        </div>
                    </div>
                    <div className="text-center">
                        <Galleria 
                            value={data?.image !== "" ? [{src: data?.image, alt: data?.name}] : []} 
                            showThumbnails={false} 
                            showIndicators
                            style={{ maxWidth: '640px' }} 
                            showIndicatorsOnItem={null} 
                            indicatorsPosition={'bottom'} 
                            item={itemGridGalleriaImageTemplate} 
                        />
                        <div className="text-2xl font-bold">{data?.name}</div>
                        <div className="mb-3">{data?.email}</div>
                        <Rating className="mb-2" value={data?.rating ?? 0} readOnly cancel={false}></Rating>
                    </div>
                    <div className="flex align-items-center justify-content-between">
                        <span className="align-self-center text-2xl font-semibold mb-2 md:align-self-end">${data?.price}</span>
                        <Button icon="pi pi-camera" className='btn-fill' label="View photographer" onClick={() => history(`/view-photographer/${data?.email}`)}></Button>
                    </div>
                </div>
            </div>
        );
    };

    const itemTemplate = (product, layout) => {
        if (!product) {
            return;
        }

        if (layout === 'list') return renderListItem ? renderListItem(product) : renderListItemDefault(product);
        else if (layout === 'grid') return renderGridItem ? renderGridItem(product) : renderGridItemDefault(product);
    };

    const renderHeader = () => {
        return (
            <div className="grid grid-nogutter">
                <div className="col-6" style={{ textAlign: 'left' }}>
                    {sortOptions && <Dropdown options={sortOptions} value={sortKey} optionLabel="label" placeholder="Sort By Price" onChange={onSortChange} />}
                </div>
                <div className="col-6" style={{ textAlign: 'right' }}>
                    <DataViewLayoutOptions layout={layout} onChange={(e) => setLayout ? setLayout(e.value) : ''} />
                </div>
            </div>
        );
    };

    const header = renderHeader();

    return (
        <div className="card w-full">
            <DataView value={data} layout={layout} header={header} itemTemplate={itemTemplate} paginator rows={rows} sortOrder={sortOrder} sortField={sortField} />
        </div>
    )
}