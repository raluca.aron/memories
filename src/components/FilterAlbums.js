import React, { useContext } from 'react'; 
import { Accordion, AccordionTab } from 'primereact/accordion';
import { UserContext } from '../contexts/UserContext';
import DropdownTemplate from './DropdownTemplate';
import { InputNumber } from 'primereact/inputnumber';

export default function FilterAlbums(props) {
    const {userData: {user}} = useContext(UserContext);
    const {
        selectedCategory,
        setSelectedCategory,
        categoryOptions,
        selectedPhotograph,
        setSelectedPhotograph,
        photographerOptions,
        selectedLocation,
        setSelectedLocation,
        locationOptions,
        ratingNumber,
        setRatingNumber,
    } = props;

    return (
        <Accordion activeIndex={0}>
            <AccordionTab header="Filter">
                <div className="grid">
                    <div className="col-12 md:col-6 lg:col-3 flex flex-column justify-content-center align-items-start">
                        <p>Category</p>
                        <DropdownTemplate
                            value={selectedCategory} 
                            onChange={(e) => setSelectedCategory ? setSelectedCategory(e.value) : ''} 
                            options={categoryOptions} 
                            optionLabel="name" 
                            placeholder="Select a option" 
                            filter
                            showClear
                        />
                    </div>
                        {
                        user?.type !== 'photographer' &&
                        <div className="col-12 md:col-6 lg:col-3 flex flex-column justify-content-center align-items-start">
                            <p>Photographer</p>
                            <DropdownTemplate
                                value={selectedPhotograph} 
                                onChange={(e) => setSelectedPhotograph ? setSelectedPhotograph(e.value) : ''} 
                                options={photographerOptions ?? []} 
                                optionLabel="name" 
                                placeholder="Select a option" 
                                filter
                                showClear 
                            />
                        </div>
                        }
                    <div className="col-12 md:col-6 lg:col-3 flex flex-column justify-content-center align-items-start">
                        <p>Location</p>
                        <DropdownTemplate
                            value={selectedLocation}
                            onChange={(e) => setSelectedLocation ? setSelectedLocation(e.value) : ''} 
                            options={locationOptions ?? []} 
                            optionLabel="name" 
                            placeholder="Select a option" 
                            filter
                            showClear
                        />
                    </div>
                    <div className="col-12 md:col-6 lg:col-3 flex flex-column justify-content-center align-items-start">
                        <p>Number of stars</p>
                        <InputNumber
                            inputId="minmax-buttons" 
                            value={ratingNumber} 
                            onValueChange={(e) => setRatingNumber ? setRatingNumber(e.value) : ''} 
                            mode="decimal" 
                            showButtons 
                            min={0} 
                            max={5} 
                        />
                    </div>
                </div>
            </AccordionTab>
        </Accordion>
    )
}