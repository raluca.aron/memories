import React, { useState } from "react";
import { Dropdown } from 'primereact/dropdown';

export default function DropdownTemplate(props) {
    const {
        value,
        onChange,
        options,
        optionLabel,
        placeholder,
        filter,
        valueTemplate,
        itemTemplate,
        showClear
    } = props;
    const [selectedElement, setSelectedElement] = useState(null);

    const selectedElementTemplate = (option, props) => {
        if (option) {
            return (
                <div className="flex align-items-center">
                    <div>{option.name}</div>
                </div>
            );
        }

        return <span>{props.placeholder}</span>;
    };

    const elementOptionTemplate = (option) => {
        return (
            <div className="flex align-items-center">
                <div>{option.name}</div>
            </div>
        );
    };

    return (
        <div className="card flex justify-content-center">
            <Dropdown 
                value={value ?? selectedElement} 
                onChange={(e) => onChange ? onChange(e) : setSelectedElement(e.value)} 
                options={options} 
                optionLabel={optionLabel ?? "name"} 
                placeholder={placeholder ?? "Select a option"} 
                filter={filter ?? false}
                valueTemplate={valueTemplate ?? selectedElementTemplate} 
                showClear={showClear ?? false}
                itemTemplate={itemTemplate ?? elementOptionTemplate} 
                className="w-full" 
            />
        </div>    
    )
}