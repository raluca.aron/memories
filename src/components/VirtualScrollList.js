import { Avatar } from 'primereact/avatar';
import { InputText } from 'primereact/inputtext';
import { VirtualScroller } from 'primereact/virtualscroller';
import React, { useContext, useState } from 'react';
import { RoutingContext } from '../contexts/RoutingContext';

const VirtualScrollList = (props) => {
    const {
        items,
        itemTemplate,
        itemSize,
        clicked,
        title
    } = props;
    const { history } = useContext(RoutingContext);
    const [searchValue, setSearchValue] = useState('');
    const itemTemplateOption = (item, options) => {
        return (
            <div key={item.id} className={`flex align-items-center justify-content-start p-2 cursor-pointer ${clicked === item?.id ? "clicked-chat" : ""}`} onClick={() => history(`/chatroom/${item.id}`)}>
                <Avatar 
                    className='p-overlay-badge'
                    label={item.label ?? ''} 
                    size="large" 
                    style={{ backgroundColor: "#8cbf7c", color: "#fff" }}
                    shape="circle" 
                />
                <div className='ml-2 p-0'>
                    <h4 className='my-0'>{item?.name ?? ''}</h4>
                </div>
            </div>
        );
    };

    return (
        <div className='w-full p-2 border-1 border-200'>
            <h2 className="font-bold block mb-2">{title}</h2>
            <span className="p-input-icon-left my-2 w-full">
                <i className="pi pi-search" />
                <InputText placeholder="Search" className='w-full' value={searchValue} onChange={(e) => setSearchValue(e.target.value)}  />
            </span>
            <VirtualScroller 
                items={items.filter(element => element.name.includes(searchValue))} 
                itemSize={itemSize ?? 0} 
                itemTemplate={itemTemplate ?? itemTemplateOption} 
                style={{ width: '100%', height: '100vh' }} 
            />
        </div>
    )
};

export default VirtualScrollList;