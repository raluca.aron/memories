import { setDoc, doc, getDoc, getDocs, collection, updateDoc, deleteField, arrayUnion, arrayRemove, deleteDoc } from "firebase/firestore"
import { firestore } from "../configuration/firebase_config"
 
export const getAlbumData = async (...args) => {

    try {
        let docSnap = undefined;
        if (args.length > 0) {
            docSnap = await getDoc(doc(firestore, ...args));
        }
        
        if (docSnap?.exists()) {
            return docSnap.data()
        } else {
            return null;
        }
    } catch(err) {
        return err
    }
}

export const getAlbumsData = async (...args) => {

        try {
            let docSnap = undefined;
            if (args.length > 0) {
                docSnap = await getDocs(collection(firestore, ...args));
            }
            
            if (docSnap) {
                return docSnap;
            } else {
                return null;
            }
        } catch(err) {
            return err
        }
}

export const updateAlbumsData = async (...args) => {
    try{
        if (args.length === 5) {
            await updateDoc(doc(firestore, args[0], args[1]), {
                [args[2]]: args[4] === 'remove' ? arrayRemove(args[3]) : 
                           args[4] === 'add' ? arrayUnion(args[3]) :
                           deleteField()
            });

            return getAlbumData(args[0], args[1]);
        } else if (args.length === 4 && args[3] === 'delete') {
            await updateDoc(doc(firestore, args[0], args[1]), {
                [args[2]]: deleteField()
            });
            return getAlbumData(args[0], args[1]);
        }

        return null;
    } catch(err) {
        return err;
    }
}

export const setAlbumData = async (...args) => {

    try {
        if (args.length === 3) {
            await setDoc(doc(firestore, args[0], args[1]), args[2]);

            return getAlbumData(args[0], args[1]);
        } else if (args.length === 4) {
            await setDoc(doc(firestore, args[0], args[1]), args[2], args[3]);

            return getAlbumData(args[0], args[1]);
        }
        
        return null;
    } catch(err) {
        return err
    }
}

export const deleteAlbumData = async (...args) => {

    try {
        if (args.length === 2) {
            return await deleteDoc(doc(firestore, args[0], args[1])); 
        } else {
            return null;
        }
    } catch(err) {
        return err;
    }
    
    
}
 