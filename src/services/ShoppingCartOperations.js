import { setDoc, doc, getDoc, arrayUnion, updateDoc, arrayRemove, deleteField, getDocs, collection } from "firebase/firestore"
import { firestore } from "../configuration/firebase_config"
 
export const getShoppingCartData = async (...args) => {

    try {
        let docSnap = undefined;
        if (args.length > 0) {
            docSnap = await getDoc(doc(firestore, ...args));
        }
        
        if (docSnap?.exists()) {
            return docSnap.data()
        } else {
            return null;
        }
    } catch(err) {
        return err
    }
}

export const getShoppingCartsData = async (...args) => {

         try {
        let docSnap = undefined;
        if (args.length > 0) {
            docSnap = await getDocs(collection(firestore, ...args));
        }
        
        if (docSnap) {
            return docSnap;
        } else {
            return null;
        }
    } catch(err) {
        return err
    }
    }

export const updateShoppingCartData = async (...args) => {
    try{
        if (args.length === 6) {
            await updateDoc(doc(firestore, args[0], args[1], args[2]), {
                [args[3]]: args[5] === 'remove' ? arrayRemove(args[4]) : 
                           args[5] === 'add' ? arrayUnion(args[4]) :
                           deleteField()
            });

            return getShoppingCartData(args[0], args[1]);
        }
        else if (args.length === 5) {
            await updateDoc(doc(firestore, args[0], args[1]), {
                [args[2]]: args[4] === 'remove' ? arrayRemove(args[3]) : 
                           args[4] === 'add' ? arrayUnion(args[3]) :
                           deleteField()
            });

            return getShoppingCartData(args[0], args[1]);
        } else if (args.length === 4 && args[3] === 'delete') {
            await updateDoc(doc(firestore, args[0], args[1]), {
                [args[2]]: deleteField()
            });
            return getShoppingCartData(args[0], args[1]);
        }

        return null;
    } catch(err) {
        return err;
    }
}

export const setShoppingCartData = async (...args) => {

    try {
        if (args.length === 3) {
            await setDoc(doc(firestore, args[0], args[1]), args[2]);

            return getShoppingCartData(args[0], args[1]);
        } else if (args.length === 4) {
            await setDoc(doc(firestore, args[0], args[1]), args[2], args[3]);

            return getShoppingCartData(args[0], args[1]);
        }
        
        return null;
    } catch(err) {
        return err
    }
}
 