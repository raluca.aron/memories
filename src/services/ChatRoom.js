import { addDoc, collection, doc, getDocs, setDoc } from "firebase/firestore";
import { firestore } from "../configuration/firebase_config"

export const getMessages = async (...args) => {
    try {
        let docSnap = undefined;
        if (args.length > 0) {
            docSnap = await getDocs(collection(firestore, ...args));
        }
        
        if (docSnap) {
            return docSnap;
        } else {
            return null;
        }
    } catch(err) {
        return err
    }
    
}

export const setMessage = async (...args) => {

        try {
            if (args.length === 4) {
                await setDoc(doc(firestore, args[0], args[1]), args[2], args[3]);

                return getMessages(args[0]);
            } else if (args.length === 2) {
                await addDoc(collection(firestore, args[0]), args[1]);
    
                return getMessages(args[0]);
            }
            return null;
        } catch(err) {
            return err
        }
    }