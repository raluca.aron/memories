import { setDoc, doc, getDoc, getDocs, collection } from "firebase/firestore"
import { firestore } from "../configuration/firebase_config"
 
export const getCategoryData = async (...args) => {

    try {
        let docSnap = undefined;
        if (args.length > 0) {
            docSnap = await getDoc(doc(firestore, ...args));
        }
        
        if (docSnap?.exists()) {
            return docSnap.data()
        } else {
            return null;
        }
    } catch(err) {
        return err
    }
}

export const getCategoriesData = async (...args) => {

        try {
            let docSnap = undefined;
            if (args.length > 0) {
                docSnap = await getDocs(collection(firestore, ...args));
            }
            
            if (docSnap) {
                return docSnap;
            } else {
                return null;
            }
        } catch(err) {
            return err
        }
    }

export const setCategoryData = async (...args) => {

    try {
        if (args.length === 3) {
            await setDoc(doc(firestore, args[0], args[1]), args[2]);

            return getCategoryData(args[0], args[1]);
        } else if (args.length === 4) {
            await setDoc(doc(firestore, args[0], args[1]), args[2], args[3]);

            return getCategoryData(args[0], args[1]);
        }
        
        return null;
    } catch(err) {
        return err
    }
}
 