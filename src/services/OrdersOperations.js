import { doc, getDoc } from "firebase/firestore";
import { firestore } from "../configuration/firebase_config"

export const getOrdersData = async (...args) => {
    try {
        let docSnap = undefined;
        if (args.length > 0) {
            docSnap = await getDoc(doc(firestore, ...args));
        }
        
        if (docSnap?.exists()) {
            return docSnap.data()
        } else {
            return null;
        }
    } catch(err) {
        return err
    }
}