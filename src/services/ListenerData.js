import { collection, doc, onSnapshot } from "firebase/firestore";
import { firestore } from "../configuration/firebase_config";

let listenerData = undefined;

export const listenerChangeCollData = (...args) => {
    return new Promise((resolve, reject) => {
        listenerData = onSnapshot(doc(firestore, ...args),
        (doc) => {
                resolve(doc.data());
        },
        (error) => {
            reject(error);
        });
    })
}

export const listenerChangeAlbumsData = (...args) => {
    return new Promise((resolve, reject) => {
        onSnapshot(collection(firestore, args[0]),
        (doc) => {
                const auxArray = [];
                if (args[1]) {
                    args[1]([]);
                }
                doc.forEach((element) => {
                    auxArray.push({
                        id: element.id,
                        name: element.data().name,
                        description: element.data().name_photograph,
                        image: element.data().gallery_photo,
                        price: element.data().price,
                        category: element.data().category,
                        rating: element.data().rating,
                        location: element.data().location
                    });
                });
                if (args[1]) {
                    args[1](auxArray);
                }
                resolve(auxArray);
        },
        (error) => {
            reject(error);
        });
    })
}

export const listenerChangeChatData = (...args) => {
    return new Promise((resolve, reject) => {
        listenerData = onSnapshot(collection(firestore, ...args),
        (doc) => {
            resolve(doc);
        },
        (error) => {
            reject(error);
        });
    })
}

export const detachListeners = () => {
    if (listenerData) {
        listenerData();
    }
}