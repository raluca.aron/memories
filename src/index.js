import React from 'react';
import ReactDOM from 'react-dom/client';
import UserProvider from './contexts/UserContext';
import './index.css';
import App from './pages/App';
import "primereact/resources/themes/lara-light-indigo/theme.css";  //theme
import "primereact/resources/primereact.min.css";                  //core css
import "primeicons/primeicons.css";
import 'primeflex/primeflex.css';      
import RoutingProvider from './contexts/RoutingContext';
import {
  BrowserRouter as Router
} from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <UserProvider>
      <Router>
        <RoutingProvider>
          <App />
        </RoutingProvider>
      </Router>
    </UserProvider>
  </React.StrictMode>
);
