import PasswordTemplate from '../components/PasswordTemplate';
import { setUserData } from '../services/UserOperations';
import { Password } from 'primereact/password';
import '../styles/Register.css';
import { InputText } from 'primereact/inputtext';
import { useRef, useState } from 'react';
import { Button } from 'primereact/button';
import { Link } from 'react-router-dom';
import { Dropdown } from 'primereact/dropdown';
import { Toast } from 'primereact/toast';
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { Timestamp } from 'firebase/firestore';
import { TOAST_TIME } from '../constants/Functions';

function Register() {

  const [email, setEmail] = useState("");

  const [password, setPassword] = useState("");

  const [selectedType, setSelectedType] = useState(null);

  const [isSubmitted, setIsSubmitted] = useState(false);

  const client_type = [
    { name: 'Photographer', code: 'photographer' },
    { name: 'Buyer', code: 'buyer' }
  ];

  const auth = getAuth();
  
  const toast = useRef(null);

  const showSuccess = () => {
    toast.current.clear();

    toast.current.show({
        severity:'success', 
        summary: 'Register with success', 
        detail:'You can now go to log in and try the app', 
        life: 3000
    });
  }

  const showWarn = (message, summary) => {
    toast.current.clear();

    toast.current.show({
        severity:'warn', 
        summary: summary ? summary : 'The account exists', 
        detail: message ? message : 'There is an account registered with this email. Please log in.', 
        life: TOAST_TIME
    });
  }

  const showError = () => {
    toast.current.clear();
    
    toast.current.show({
        severity:'error', 
        summary: 'Register failed', 
        detail:'The account can\'t be created', 
        life: TOAST_TIME
    });
  }

  const resetSubmittedStatus = () => {
    setTimeout(() => {
        setIsSubmitted(false);
    }, TOAST_TIME);
  }

  const handleRegister = () => {
    setIsSubmitted(true);
    if (email !== "" && password !== "" && selectedType !== null && email.match(/(@[a-zA-Z]+.[a-zA=Z]+)$/gm)) {
        createUserWithEmailAndPassword(auth, email, password)
        .then(async () => {
            let bodyData = {
                firstLogin: true,
                firstName: "",
                lastName: "",
                image: "",
                isLogged: true,
                lastLogged: Timestamp.now(),
                location: "",
                email: email,
                type: selectedType,
                permissions: selectedType !== 'photographer' ? ['/home', '/profile', '/view-photographer', '/shopping-cart', '/orders', '/favorites', '/chatroom'] : 
                ['/profile', '/orders', '/chatroom']
            };
            if (selectedType === "photographer") {
                bodyData.description = "";
                bodyData.gallery_photo = [];
                bodyData.hour_rate = 0;
                bodyData.rating = [];
                bodyData.checkedAccount = false;
                bodyData.socialMedia = "";
            }
            await setUserData("users", email, bodyData).then(res => {
                if (res) {
                    setEmail('');
                    setPassword('');
                    setSelectedType(null);
                    showSuccess();
                } else {
                    showError();
                }
            })
        })
        .catch((error) => {
            if (error.code === "auth/email-already-in-use") {
                showWarn();
                setEmail("");
                setPassword("");
                setSelectedType(null);
            } else {
                showError();
                setEmail("");
                setPassword("");
                setSelectedType(null);
            }
        });

    } else if (email === "" && password === "" && (selectedType === null || selectedType !== null)) {
        showWarn("You need to provide an valid credentials", "Invalid data");
    } else if (email === "") {
        showWarn("You need to provide an email", "Invalid email");
    } else if (!email.match(/((\@)[a-zA-Z]+(\.)[a-zA=Z]+)$/gm)) {
        showWarn("You need to provide a valid email", "Invalid email");
        setEmail("");
    } else if (selectedType === null) {
        showWarn("You need to select an account type", "Invalid Account Type");
    } else if (password === "") {
        showWarn("You need to provide a password", "Invalid Password");
    }

    resetSubmittedStatus();
  }

  const userTypeOptionTemplate = (option) => {
    return (
        <div className="align-center" style={{flexDirection: 'row'}}>
            <i className={option.code === 'photographer' ? "pi pi-camera" : 'pi pi-dollar'} />
            <div className='margin-left-sm'>{option.name}</div>
        </div>
    );
  }

  const selectedUserTypeTemplate = (option, props) => {
    if (option) {
        return (
            <div className="align-center" style={{flexDirection: 'row'}}>
                <i className={option.code === 'photographer' ? "pi pi-camera" : 'pi pi-dollar'} />
                <div className='margin-left-sm'>{option.name}</div>
            </div>
        );
    }

    return (
        <span>
            {props.placeholder}
        </span>
    );
  }

  return (
    <div className="Register align-center">
        <div className='form-register align-center shadow-5'>
            <div className='align-left'>
                <h3>Email</h3>
                <span className="p-input-icon-left">
                    <i className="pi pi-user" />
                    <InputText className={isSubmitted && email === "" ? 'p-invalid' : ""} value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Email" />
                </span>
            </div>
            <div className='align-left margin-bottom-between'>
                <h3>Account type</h3>
                <Dropdown 
                    className={`match-parent ${isSubmitted && selectedType === null ? 'p-invalid' : ""}`}
                    value={selectedType}
                    options={client_type}
                    onChange={(e) => setSelectedType(e.value)} 
                    optionLabel="name"
                    optionValue='code'
                    itemTemplate={userTypeOptionTemplate}
                    valueTemplate={selectedUserTypeTemplate}
                    placeholder="Select a user type" />
            </div>
            <div className='align-left margin-bottom-between'>
                <h3>Password</h3>
                <Password 
                    value={password} 
                    placeholder='Password'
                    className={isSubmitted && password === "" ? 'p-invalid' : ""}
                    onChange={(e) => setPassword(e.target.value)} 
                    header={<h6>Pick a password</h6>} 
                    footer={PasswordTemplate} 
                    toggleMask />
            </div>
            <div className='align-center margin-bottom-between'>
                <Button label="Sign Up" className="btn-white" onClick={() => handleRegister()}/>
            </div>
            <div className='align-center margin-bottom-between'>
                <p className='margin-zero'>Do you already have an account?</p>
                <Link to='/' className='text-white no-underline mt-3'>Log in now</Link>
            </div>
        </div>
        <Toast ref={toast} />
    </div>
  );
}

export default Register;
