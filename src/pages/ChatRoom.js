import { Button } from 'primereact/button';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';
import ChatMessage from '../components/ChatMessage';
import { UserContext } from '../contexts/UserContext';
import { getAlbumsData } from '../services/AlbumOperations';
import { listenerChangeChatData } from '../services/ListenerData';
import { Timestamp } from '@firebase/firestore';
import { Avatar } from 'primereact/avatar';
import '../styles/ChatRoom.css';
import { getUserData, setUserData } from '../services/UserOperations';
import moment from 'moment';
import { Badge } from 'primereact/badge';
import OfferDialog from '../components/OfferDialog';
import { getShoppingCartData, setShoppingCartData } from '../services/ShoppingCartOperations';
import VirtualScrollList from '../components/VirtualScrollList';
import { getCategoriesData } from '../services/CategoryOperations';
import { Message } from 'primereact/message';
import { useWindowSize } from '../constants/Functions';

const ChatRoom = () => {

    const {nameConversation} = useParams();

    const [chatMessages, setChatMessages] = useState([]);

    const [chat, setChat] = useState([]);

    const [messageChat, setMessageChat] = useState('');

    const [width, height] = useWindowSize();

    const [categoryOptions, setCategoryOptions] = useState([]);

    const { userData: {user} } = useContext(UserContext);

    const [activePeriod, setActivePeriod] = useState({
        time: 1,
        unit: 'second'
    });

    const [photographer, setPhotographer] = useState(null);

    const dummyRef = useRef(null);

    const [visibleDialog, setVisibleDialog] = useState(false);

    const handleGetPhotoMessagesList = async () => {
        await getAlbumsData("messages")
        .then(res => {
            if (!res?.hasOwnProperty("customData")) {
                res?.forEach(element => {
                    if (element.id.split(":")[0] === user?.email) {
                        setChatMessages([...chatMessages, { id: element.id }]);
                    }
                })
            }
        })
    };

    const handleGetBuyerMessagesList = async () => {
        await getAlbumsData("messages")
        .then(res => {
            if (!res?.hasOwnProperty("customData")) {
                res?.forEach(element => {
                    if (element.id.split(":")[1] === user?.email) {
                        setChatMessages([...chatMessages, { id: element.id }]);
                    }
                })
            }
        })
    };


    const handleGetPhotographerData = async () => {
        if (nameConversation && user) {
            await getUserData("users", user?.type === "photographer" ? nameConversation.split(":")[1] : nameConversation.split(":")[0]).then(res => {
                if (res) {
                    let loggedDate = new Date(res?.lastLogged.seconds * 1000 + res?.lastLogged.nanoseconds / 1000000);
                    const startDate = moment(loggedDate);
                    const endDate = moment(new Date());
                    const diff = endDate.diff(startDate);
                    const diffDuration = moment.duration(diff);
                    let period = 1;
                    let unitPeriod = 'second'
                    if (diffDuration.seconds() > 1) {
                        period = diffDuration.seconds();
                        unitPeriod = 'seconds';
                    }

                    if (diffDuration.minutes() > 0) {
                        period = diffDuration.minutes();
                        if (diffDuration.minutes() === 1)
                            unitPeriod = 'minute';
                        else
                            unitPeriod = 'minutes';
                    }

                    if (diffDuration.hours() > 0) {
                        period = diffDuration.hours();
                        if (diffDuration.hours() === 1)
                            unitPeriod = 'hour';
                        else
                            unitPeriod = 'hours';
                    }

                    if (diffDuration.days() > 0) {
                        period = diffDuration.days();
                        if (diffDuration.days() === 1)
                            unitPeriod = 'day';
                        else
                            unitPeriod = 'days';
                    }

                    setActivePeriod({
                        time: period,
                        unit: unitPeriod
                    });

                    res.email = nameConversation.split(":")[0];
                    setPhotographer({
                        ...res
                    });
                } else {
                    setPhotographer(null);
                }
    
            })
        }
    }

    const handleGetMessages = async () => {
        await getUserData("messages", nameConversation)
                .then(res => {
                    if (res?.hasOwnProperty("messages")) {
                        if (res?.messages?.filter(element => !chat?.map(msg => msg?.text).includes(element?.text)).length > 0) {
                            setChat(res?.messages);
                            if (dummyRef) {
                                dummyRef.current.scrollIntoView({ behavior: 'smooth' });
                            }
                        }
                    }
                })
    }

    const handleGetCategoriesData = async () => {
        await getCategoriesData("category").then(res => {
            if (!res?.hasOwnProperty("code") && res) {
                const auxArray = [];
                res?.forEach(element => {
                    auxArray.push({
                      code: element.id,
                      name: element.data().name
                    })
                });
                setCategoryOptions(auxArray);
            }
      })
    }

    useEffect(() => {
        if (user?.type === "photographer" && user?.checkedAccount) {
            handleGetPhotoMessagesList();
            handleGetCategoriesData();
        } else if (user?.type === "buyer") {
            handleGetBuyerMessagesList();
        }

    }, [chatMessages.length]);


    useEffect(() => {
        if (nameConversation && chatMessages?.length === 0) {
            handleGetMessages();
        }

        if (!photographer && nameConversation) {
            handleGetPhotographerData();
        }
    }, [nameConversation]);

    useEffect(() => {
        if (nameConversation && ((user?.checkedAccount && user?.type === "photographer") || user?.type !== "photographer")) {
            listenerChangeChatData("messages")
            .then(res => {
            if (res) {
                handleGetMessages();
                if (user?.type === "photographer") {
                    handleGetPhotoMessagesList();
                } else {
                    handleGetBuyerMessagesList();
                }
            }
            });
        }
    })

    const sendMessage = async (e) => {
        e.preventDefault();
        if (user && photographer && nameConversation && messageChat) {
            await setUserData('messages', nameConversation, {
                messages: [...chat, {
                    text: messageChat,
                    isOffer: false,
                    sender: user?.email,
                    receiver: photographer?.email,
                    createdAt: Timestamp.now(),
                }]
            }, { merge: true })
            .then(res => {
                if (res?.hasOwnProperty("messages")) {
                    setMessageChat("");
                    setChat(res?.messages ?? []);
                    if (dummyRef) {
                        dummyRef.current.scrollIntoView(false, { behavior: 'smooth' });
                    }
                }
                setVisibleDialog(false);
            })
        }
    }

    const handleSetOffer = async (data) => {
        if (user && photographer && nameConversation) {
            await setUserData('messages', nameConversation, {
                messages: [...chat, {
                    text: JSON.stringify(data),
                    isOffer: true,
                    sender: user?.email,
                    receiver: photographer?.email,
                    createdAt: Timestamp.now(),
                }]
            }, { merge: true })
            .then(res => {
                if (res?.hasOwnProperty("messages")) {
                    setChat(res?.messages ?? []);
                    if (dummyRef) {
                        dummyRef.current.scrollIntoView(false, { behavior: 'smooth' });
                    }
                }
                setVisibleDialog(false);
            })
        }
    }

    const handleSetOfferStatus = async (status, message) => {
        if (user?.type === "buyer" && photographer && nameConversation && message) {
            const text = JSON.parse(message.text);
            text.isAccepted = status;
            const newChat = [];
            chat.map(element => {
                if (element.isOffer && JSON.parse(element.text).packageName !== text.packageName) {
                    newChat.push(element);
                } else if (!element.isOffer) {
                    newChat.push(element);
                }
            });
            await setUserData('messages', nameConversation, {
                messages: [...newChat, {
                    text: JSON.stringify(text),
                    isOffer: true,
                    sender: message.sender,
                    receiver: message.receiver,
                    createdAt: message.createdAt,
                }]
            }, { merge: true })
            .then(async res => {
                if (res?.hasOwnProperty("messages")) {
                    setChat(res?.messages ?? []);
                    if (dummyRef) {
                        dummyRef.current.scrollIntoView(false, { behavior: 'smooth' });
                    }
                    if (status) {
                        await getShoppingCartData("shopping-cart", "shopping-chart-new").then(async res => {
                            if (res?.hasOwnProperty(user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase())) {
                                const newCart = [...res[user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()].cart];
                                if (newCart.filter(element => element.name_package === text.packageName)?.length === 0) {
                                    newCart.push({
                                        category: text?.categoryOptions.code ?? '',
                                        id_photograph: photographer?.email,
                                        id_user: user?.email,
                                        name_package: text?.packageName,
                                        name_photograph: photographer?.firstName + " " + photographer?.lastName,
                                        price: text?.price,
                                        image: null,
                                    });
                                    await setShoppingCartData("shopping-cart", "shopping-chart-new", 
                                    {
                                        [user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()]: {
                                            ...res[user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()],
                                            cart: newCart
                                        }
                                    });
                                }

                            } else {
                                await setShoppingCartData("shopping-cart", "shopping-chart-new", {
                                    [user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()]: {
                                        isPaid: false,
                                        address: '',
                                        calendar: '',
                                        location: photographer?.location,
                                        cart: [
                                            {
                                                category: text?.categoryOptions.code ?? '',
                                                id_photograph: photographer?.email,
                                                id_user: user?.email,
                                                name_package: text?.packageName,
                                                name_photograph: photographer?.firstName + " " + photographer?.lastName,
                                                price: text?.price,
                                                image: null,
                                            }
                                        ]
                                    }
                                })
                            }
                        })
                    }                    
                }
            })
        }
    }


    if (width < 768)
        if (chat && nameConversation)
            return (<div className='pb-4'>
                <div className='flex align-items-center justify-content-start p-2' style={{boxShadow: '0 5px 12px 0px #dadada'}}>
                    <Avatar 
                        className='p-overlay-badge'
                        label={photographer?.firstName?.[0]?.toUpperCase() ?? ''} 
                        size="large" 
                        style={{ backgroundColor: "#8cbf7c", color: "#fff" }} 
                        shape="circle" 
                    >
                        {photographer?.isLogged && <Badge value="" severity='success' style={{top: '90%', right: '25%', width: '16px', height: '16px', border: '3px solid #fff'}} />}
                    </Avatar>
                    <div className='ml-2 p-0'>
                        <h4 className='my-0'>{(photographer?.firstName ?? "") + " " + (photographer?.lastName ?? "") ?? ''}</h4>
                        <h5 className='my-0 font-normal'>{photographer?.isLogged ? 'Active now' : `Active ${activePeriod?.time + ' ' + activePeriod.unit} ago`}</h5>
                    </div>
                </div>
                <div className='overflow-y-auto p-2 chat-body'>
                    {chat?.sort((firstComment, secondComment) => {
                        if (firstComment?.createdAt > secondComment?.createdAt)
                            return 1;
                        
                        if (firstComment?.createdAt < secondComment?.createdAt)
                            return -1;
                        
                        return 0;
                    })
                    ?.map((msg, index) => <ChatMessage key={`message-${index}`} message={msg} />)}
                    <div ref={dummyRef}></div>
                </div>
                <div className='grid'>
                    <div className={user?.type === "photographer" ? 'col-12 md:col-10 lg:col-10' : "col-12"}>
                        <form onSubmit={sendMessage} className="w-full flex pr-2">
                            <input 
                                value={messageChat} 
                                onChange={(e) => setMessageChat(e.target.value)} 
                                className="flex-grow-1 input-form-chat"
                                placeholder='Aa'
                            />
                            <Button type="submit" label='Send' className='btn-fill'/>
                        </form>
                    </div>
                    {user?.type === "photographer" && 
                    <div className='col-12 md:col-2 lg:col-2'>
                        <Button label='Offer' className='btn-white' onClick={() => setVisibleDialog(true)} />
                    </div>}
                    
                </div>
            </div>)
        else
            return (
            <div className='align-center h-full'>
                {!user?.checkedAccount && user?.type === "photographer" && <Message severity="error" className='my-4' text="Your account isn't checked, please wait until the admin confirm your account" />}
                <VirtualScrollList
                    items={(chatMessages ?? []).sort((firstComment, secondComment) => {
                        if (new Date(firstComment?.createdAt?.seconds * 1000 + firstComment?.createdAt?.nanoseconds / 1000000)
                            < 
                        new Date(secondComment?.createdAt?.seconds * 1000 + secondComment?.createdAt?.nanoseconds / 1000000)
                        )
                            return 1;
                        
                        if (new Date(firstComment?.createdAt?.seconds * 1000 + firstComment?.createdAt?.nanoseconds / 1000000)
                            > 
                            new Date(secondComment?.createdAt?.seconds * 1000 + secondComment?.createdAt?.nanoseconds / 1000000))
                            return -1;
                        
                        return 0;
                    }).filter((message, index) => {
                        return index === chatMessages
                        ?.findIndex(obj => obj.id === message.id)
                    })
                    ?.map(element => 
                        ({ id: element?.id,
                        label: user?.type === "photographer" ? element?.id?.split("_")[1][0].toUpperCase() : element?.id[0][0].toUpperCase(),
                        name: user?.type === "photographer" ? element?.id?.split("_")[1] : element?.id?.split("_")[0]}))}
                    itemSize={50}
                    clicked={nameConversation}
                    title="Conversations"
                />
            </div>
            )
    else return (
        <div className="grid m-0">
            {
                !user?.checkedAccount && user?.type === "photographer" &&
                <div className='col-12 align-center'>
                    <Message severity="error" className='my-4' text="Your account isn't checked, please wait until the admin confirm your account" />
                </div>
            }
            <div className='col-12 md:col-4 lg:col-3 pr-0 h-full'>
                <VirtualScrollList
                    items={(chatMessages ?? []).sort((firstComment, secondComment) => {
                        if (new Date(firstComment?.createdAt?.seconds * 1000 + firstComment?.createdAt?.nanoseconds / 1000000)
                            < 
                        new Date(secondComment?.createdAt?.seconds * 1000 + secondComment?.createdAt?.nanoseconds / 1000000)
                        )
                            return 1;
                        
                        if (new Date(firstComment?.createdAt?.seconds * 1000 + firstComment?.createdAt?.nanoseconds / 1000000)
                            > 
                            new Date(secondComment?.createdAt?.seconds * 1000 + secondComment?.createdAt?.nanoseconds / 1000000))
                            return -1;
                        
                        return 0;
                    }).filter((message, index) => {
                        return index === chatMessages
                        ?.findIndex(obj => obj.id === message.id)
                    })
                    ?.map(element => 
                        ({ id: element?.id,
                        label: user?.type === "photographer" ? element?.id?.split(":")[1][0].toUpperCase() : element?.id[0][0].toUpperCase(),
                        name: user?.type === "photographer" ? element?.id?.split(":")[1] : element?.id?.split(":")[0]}))}
                    itemSize={50}
                    clicked={nameConversation}
                    title="Conversations"
                />
                
                
                <OfferDialog
                    open={visibleDialog}
                    categoryOptions={ categoryOptions }
                    onHide={() => setVisibleDialog(false)}
                    onOfferFinished={(data) => handleSetOffer(data)}
                />
            </div>
            {
                chat && nameConversation &&
                <div className='col-12 md:col-8 lg:col-9 pl-0'>
                    <div className='flex align-items-center justify-content-start p-2' style={{boxShadow: '0 5px 12px 0px #dadada'}}>
                        <Avatar 
                            className='p-overlay-badge'
                            label={photographer?.firstName?.[0]?.toUpperCase() ?? ''} 
                            size="large" 
                            style={{ backgroundColor: "#8cbf7c", color: "#fff" }} 
                            shape="circle" 
                        >
                            {photographer?.isLogged && <Badge value="" severity='success' style={{top: '90%', right: '25%', width: '16px', height: '16px', border: '3px solid #fff'}} />}
                        </Avatar>
                        <div className='ml-2 p-0'>
                            <h4 className='my-0'>{(photographer?.firstName ?? "") + " " + (photographer?.lastName ?? "") ?? ''}</h4>
                            <h5 className='my-0 font-normal'>{photographer?.isLogged ? 'Active now' : `Active ${activePeriod?.time + ' ' + activePeriod.unit} ago`}</h5>
                        </div>
                    </div>
                    <div className='overflow-y-auto p-2 chat-body'>
                        {chat?.sort((firstComment, secondComment) => {
                            if (firstComment?.createdAt > secondComment?.createdAt)
                                return 1;
                            
                            if (firstComment?.createdAt < secondComment?.createdAt)
                                return -1;
                            
                            return 0;
                        })
                        ?.map((msg, index) => <ChatMessage key={`message-${index}`} message={msg} photoView={user?.type === "photographer" ? true : false} onOfferAction={(status) => handleSetOfferStatus(status, msg)} />)}
                        <div ref={dummyRef}></div>
                    </div>
                    <div className='grid'>
                        <div className={user?.type === "photographer" ? 'col-12 md:col-10 lg:col-10' : "col-12"}>
                            <form onSubmit={sendMessage} className="w-full flex pr-2">
                                <input 
                                    value={messageChat} 
                                    onChange={(e) => setMessageChat(e.target.value)} 
                                    className="flex-grow-1 input-form-chat"
                                    placeholder='Aa'
                                />
                                <Button type="submit" label='Send' className='btn-fill'/>
                            </form>
                        </div>
                        {user?.type === "photographer" && 
                        <div className='col-12 md:col-2 lg:col-2'>
                            <Button label='Offer' className='btn-white' onClick={() => setVisibleDialog(true)} />
                        </div>}
                        
                    </div>
                </div>
            }
        </div>
    );
};

export default ChatRoom;