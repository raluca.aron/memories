import React from "react";
import { Button } from 'primereact/button';
import { useEffect, useRef, useState } from 'react';
import { deleteUserData, getUsersData, setUserData } from '../services/UserOperations';
import '../styles/Profile.css';
import { Toast } from 'primereact/toast';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { listenerChangeChatData } from "../services/ListenerData";
import { InputText } from "primereact/inputtext";

function Categories() {

 const toast = useRef(null);

 const [category, setCategory] = useState([]);

 const [categoryType, setCategoryType] = useState("");
 
 const [categoryName, setCategoryName] = useState("");

 const showSuccess = () => {
    if (toast)
        toast.current.show({severity:'success', summary: 'Operation succeeded', detail:'Data has been modified', life: 3000});
 };

const showError = () => {
    if (toast)
        toast.current.show({severity:'error', summary: 'Operation failed', detail:'Data hasn\'t been modified', life: 3000});
}

    const handleGetCategoriesData = async () => {
        await getUsersData("category").then(res => {
            const auxArray = [];
            res?.forEach(element => {
                auxArray.push({
                    id: element.id,
                    name: element.data().name
                });
            });
            setCategory(auxArray);
        });
    }

const handleDeleteCategory = async (id) => {
    await deleteUserData("category", id).then(res => {
        showSuccess();
    });
}

 useEffect(() => {
    handleGetCategoriesData();
 }, []);

 useEffect(() => {
    listenerChangeChatData("category")
    .then(res => {
        if (res) {
            const auxArray = []
            res?.forEach(element => {
                auxArray.push({
                    id: element.id,
                    name: element.data().name
                });
            });
            setCategory(auxArray);
        }
    })
 })

 const handleAddCategory = async () => {
    let bodyData = {}

    if (categoryName !== '') {
        bodyData.name = categoryName;
    }

    if (Object.keys(bodyData).length > 0 && categoryType !== '') {
        await setUserData("category", categoryType, bodyData, { merge: true })
        .then(res => {
            if (res) {  
                showSuccess();
                setCategory([...category, {id: categoryType, name: categoryName}]);
                setCategoryName("");
                setCategoryType("");
            }
        })
        .catch(() => {
            showError();
        })
    }
 }

 const catBodyTemplate = (rowData) => {
    return (
        <React.Fragment>
            <Button label="Delete" className="btn-fill" onClick={() => handleDeleteCategory(rowData.id)} />
        </React.Fragment>
    );
 };

  return (
    <div className="align-center m-0 p-2">
        <h2>Category</h2>
        <DataTable value={category ?? []} tableStyle={{ width: '100%' }}>
            <Column field="id" header="Id"></Column>
            <Column field="name" header="Name"></Column>
            <Column body={ catBodyTemplate } header="Action" exportable={false} style={{ minWidth: '5rem' }}></Column>
        </DataTable>
        <div className='mt-6 grid w-10 lg:w-8 xl:w-6 gap-2 align-center border-1 border-solid border-round border-400 shadow-6 mb-8'>
            <div className='col-12 align-left bg-green-custom pl-6'>
                <h2 className='text-white'>
                    Event Category
                </h2>
            </div>
            <div className='col-11 grid'>
                <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left'>
                    <h5>Category type</h5>
                    <span className="p-input-icon-left">
                        <i className="pi pi-book" />
                        <InputText
                            value={categoryType}
                            onChange={(e) => setCategoryType(e.target.value)} 
                            placeholder="Category type" />
                    </span>
                </div>
                <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left' style={{ justifyContent: 'flex-start' }}>
                    <h5>Category name</h5>
                    <span className="p-input-icon-left">
                        <i className="pi pi-id-card" />
                        <InputText 
                            value={categoryName}
                            onChange={(e) => setCategoryName(e.target.value)} 
                            placeholder="Category name" />
                    </span>
                </div>
            </div>
            <div className='col-11 mb-2'>
                <Button label="Add event category" className='btn-fill' onClick={() => handleAddCategory()} />
            </div>
        </div> 
        <Toast ref={toast} />
    </div>
  );
}

export default Categories;