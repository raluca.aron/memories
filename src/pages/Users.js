import React from "react";
import { Button } from 'primereact/button';
import { useEffect, useRef, useState } from 'react';
import { deleteUserData, getUsersData, setUserData } from '../services/UserOperations';
import '../styles/Profile.css';
import { Toast } from 'primereact/toast';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { listenerChangeChatData } from "../services/ListenerData";

function Users() {

 const toast = useRef(null);

 const [users, setUsers] = useState([]);

 const showSuccess = () => {
    if (toast)
        toast.current.show({severity:'success', summary: 'Operation succeeded', detail:'Data has been modified', life: 3000});
 };

    const handleGetUsersData = async () => {
        await getUsersData("users").then(res => {
            const auxArray = []
            res?.forEach(element => {
                auxArray.push(element.data());
            });
            setUsers(auxArray);
        });
    }

const handleDeleteUser = async (email) => {
    await deleteUserData("users", email).then(res => {
        showSuccess();
    });
}

const handleVerifyUser = async (email) => {
    await setUserData("users", email, { checkedAccount: true }, { merge: true })
    .then(res => {
        if (res) {  
            showSuccess();
        }
    })
}

 useEffect(() => {
    handleGetUsersData();
 }, []);

 useEffect(() => {
    listenerChangeChatData("users")
    .then(res => {
        if (res) {
            const auxArray = []
            res?.forEach(element => {
                auxArray.push(element.data());
            });
            setUsers(auxArray);
        }
    })
 });

 const buyerBodyTemplate = (rowData) => {
    return (
        <React.Fragment>
            <Button label="Delete" className="btn-fill" onClick={() => handleDeleteUser(rowData.email)} />
        </React.Fragment>
    );
 };
 
 const photographerBodyTemplate = (rowData) => {
    return (
        <React.Fragment>
            <Button label="Check account" disabled={ rowData.checkedAccount } className="btn-fill mr-2" onClick={() => handleVerifyUser(rowData.email)} />
            <Button label="Delete" className="btn-fill" onClick={() => handleDeleteUser(rowData.email)} />
        </React.Fragment>
    );
 };

  return (
    <div className="align-center w-full m-0 p-2">
        <h2>Buyer users</h2>
        <DataTable value={(users ?? []).filter(element => element?.type === "buyer")} tableStyle={{ width: '100%' }}>
            <Column field="firstName" header="First Name"></Column>
            <Column field="lastName" header="Last Name"></Column>
            <Column field="email" header="Email"></Column>
            <Column field="location" header="Location"></Column>
            <Column field="firstLogin" header="First Login"></Column>
            <Column body={ buyerBodyTemplate } header="Action" exportable={false} style={{ minWidth: '5rem' }}></Column>
        </DataTable>
        <h2 className='mt-4'>Photographer users</h2>
        <DataTable value={(users ?? []).filter(element => element?.type === "photographer")} tableStyle={{ width: '100%' }}>
            <Column field="firstName" header="First Name"></Column>
            <Column field="lastName" header="Last Name"></Column>
            <Column field="email" header="Email"></Column>
            <Column field="location" header="Location"></Column>
            <Column field="firstLogin" header="First Login"></Column>
            <Column field="socialMedia" header="Social Media" style={{ wordBreak: "break-word" }}></Column>
            <Column body={ photographerBodyTemplate } header="Action" exportable={false} style={{ width: '20rem' }}></Column>
        </DataTable>
        <Toast ref={toast} />
    </div>
  );
}

export default Users;