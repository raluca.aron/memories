import React, { useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import { Button } from "primereact/button";
import DataViewTemplate from "../components/DataViewTemplate";
import { getShoppingCartData, setShoppingCartData } from "../services/ShoppingCartOperations";
import { Toast } from "primereact/toast";
import PaymentDialog from "../components/PaymentDialog";
import PaymentDetails from "../components/PaymentDetails";
import CheckoutDialog from "../components/CheckoutDialog";
import CheckoutDetails from "../components/CheckoutDetails";
import { handleUpdateShoppingCartData } from "../constants/Functions";

const ShoppingCart = () => {
 
    const [shoppingList, setShoppingList] = useState(null);

    const [visiblePayDialog, setVisiblePayDialog] = useState(false);

    const [checkoutData, setCheckoutData] = useState(undefined);

    const [visibleCheckout, setVisibleCheckout] = useState(false);

    const toast = useRef(null);

    const {cartName} = useParams();

    const handleGetShoppingCartData = async (document, cartName) => {
        await getShoppingCartData("shopping-cart", document).then(res => {
            if (res?.hasOwnProperty(cartName)) {
                setShoppingList(res?.[cartName] ?? null);
            }
        })
    }

    useEffect(() => {
        if (cartName) {
            handleGetShoppingCartData("shopping-chart-new", cartName);
            setCheckoutData({
                address: undefined,
                location: shoppingList?.location ?? undefined,
                info: shoppingList?.info ?? undefined
            });
        }
    }, [shoppingList?.cart?.length]);

    const showSuccess = () => {
        if (toast)
            toast.current.show({severity:'success', summary: 'Operation succeeded', detail:'Data has been modified', life: 3000});
    };

    const showError = () => {
        if (toast)
            toast.current.show({severity:'error', summary: 'Operation failed', detail:'Data hasn\'t been modified', life: 3000});
    };

    const handleDeleteAlbum = async (document, cartName, elementFromArray) => {
        if (elementFromArray) {
            await setShoppingCartData("shopping-cart", "shopping-chart-new", 
            {
                [cartName]: {
                    ...shoppingList,
                    cart: shoppingList?.cart?.filter(element => element?.name_package !== elementFromArray?.name_package)
                }
            })
            .then(res => {
                setShoppingList([]);
                showSuccess();
            })
            .catch(error => {
                showError();
            })
        }
    }

    const handlePayEvent = async (data) => {
        if (shoppingList?.cart?.length > 0 && cartName && checkoutData) {
            await setShoppingCartData("shopping-cart", "shopping-chart-new", 
            {
                [cartName]: {
                    ...shoppingList,
                    calendar: checkoutData?.calendar,
                    location: checkoutData?.location,
                    address: checkoutData?.address,
                    isPaid: true
                }
            })
            .then(async res => {
                await setShoppingCartData("shopping-cart", "shopping-cart-old", 
                {
                    [cartName + '-' + new Date().toLocaleDateString("en-US").split('/').join('-') + '-' + new Date().toLocaleTimeString().replace(' AM', '').split(":").join('-')]: {
                        ...shoppingList,
                        isPaid: true,
                        calendar: checkoutData?.calendar,
                        location: checkoutData?.location,
                        address: checkoutData?.address,
                        cart: shoppingList?.cart?.map(element => {
                            element.isRejected = false;
                            element.reason = '';
                            element.isFinished = false;

                            return element;
                        })
                    }
                }, { merge: true })
                .then(async res => {
                    await handleUpdateShoppingCartData("shopping-chart-new", cartName, "delete")
                    .then(response => {
                        toast.current.clear();
                        toast.current.show({severity:'success', summary: 'Pay succeeded', detail:'your payment has been accepted', life: 3000});
                        setTimeout(() => {
                            setVisiblePayDialog(false);
                            setCheckoutData(undefined);
                            setVisibleCheckout(false);
                            setShoppingList([]);
                        }, 3000);
                    })
                })
                .catch(error => {
                    showError();
                })
            })
            .catch(error => {
                showError();
            })
        }
    }

    const handleCheckoutData = (data) => {
        setCheckoutData(data);
        setVisiblePayDialog(true);
    }

    return (
        <div className="grid p-3 m-0">
             <DataViewTemplate
            data={shoppingList?.cart ?? []}
            layout="list"
            renderListItem={(data) => (<div className="col-12">
                <div className="flex flex-column align-items-center p-3 w-full md:flex-row">
                    {data?.image && <img className="md:w-11rem w-9 shadow-2 md:my-0 md:mr-5 mr-0 my-5 w-full h-7rem" src={data?.image} alt={data.name_package} />}
                    <div className="text-center md:text-left md:flex-1">
                        <div className="text-2xl font-bold">{data?.name_package}</div>
                        <div className="mb-3">{data?.name_photograph}</div>
                    </div>
                    <div className="flex md:flex-column mt-5 justify-content-between align-items-center md:w-auto w-full">
                        <span className="align-self-center text-2xl font-semibold mb-2 md:align-self-end">Price</span>
                        <span className="align-self-center text-2xl font-semibold mb-2 md:align-self-end">${data?.price}</span>
                        <Button className="btn-fill mb-2" icon="pi pi-times" label="Remove Package" onClick={() => handleDeleteAlbum("shopping-chart-new", cartName, data)}></Button>
                    </div>
                </div>
            </div>)}
            rows={9}
          />
          <CheckoutDetails
            data={checkoutData}
          />
          <div>
            <Button label='Checkout Details' className="btn-fill" onClick={() => setVisibleCheckout(true)}/>
            <CheckoutDialog
                data={checkoutData}
                open={visibleCheckout}
                onHide={() => setVisibleCheckout(false)}
                onCompleteAddress={(data) => handleCheckoutData(data)}
            />
          </div>
          <PaymentDetails
            totalOrder={(shoppingList?.cart?.reduce((accumulator, currentElement) => accumulator + currentElement.price, 0)) ?? 0}
          />
          <div>
            <Button label={`Pay $${(shoppingList?.cart?.reduce((accumulator, currentElement) => accumulator + currentElement.price, 0)) ?? 0}`}
                disabled={(shoppingList?.cart?.reduce((accumulator, currentElement) => accumulator + currentElement.price, 0) ? false : true) || !checkoutData?.address || !checkoutData?.calendar}
                className="btn-fill"
                onClick={() => setVisiblePayDialog(true)}
            />
            <PaymentDialog
                open={visiblePayDialog}
                onHide={() => setVisiblePayDialog(false)}
                total={(shoppingList?.cart?.reduce((accumulator, currentElement) => accumulator + currentElement.price, 0))}
                onPayEvent={handlePayEvent}
            />
          </div>
          <div>
              <Toast ref={toast} />
          </div>
        </div>
    )
}

export default ShoppingCart;