import React, { useContext, useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import { Button } from "primereact/button";
import DataViewTemplate from "../components/DataViewTemplate";
import { getShoppingCartData, setShoppingCartData } from "../services/ShoppingCartOperations";
import { Toast } from "primereact/toast";
import { Accordion, AccordionTab } from "primereact/accordion";
import { UserContext } from "../contexts/UserContext";
import { Chip } from 'primereact/chip';
import AddFeedbackDialog from "../components/AddFeedbackDialog";
import { handleUpdateShoppingCartData } from "../constants/Functions";

const Orders = () => {

    const [shoppingList, setShoppingList] = useState([]);

    const [orderRejected, setOrderRejected] = useState(undefined);

    const [packageRejected, setPackageRejected] = useState(undefined);

    const [showReasonDialog, setShowReasonDialog] = useState(false);

    const {userData: {user}} = useContext(UserContext);

    const {userName} = useParams();

    const toast = useRef(null);

    const showSuccess = () => {
        if (toast)
            toast.current.show({severity:'success', summary: 'Operation succeeded', detail:'Data has been modified', life: 3000});
    };

    const showWarn = (message) => {
        if (toast)
            toast.current.show({severity:'warn', summary: 'Warning', detail: message, life: 3000});
    };

    const showError = () => {
        if (toast)
            toast.current.show({severity:'error', summary: 'Operation failed', detail:'Data hasn\'t been modified', life: 3000});
    }

    const handleDeleteOrder = async (id_order) => {
        if (id_order !== "") {
            await handleUpdateShoppingCartData("shopping-cart-old", id_order, "delete")
            .then(response => {
                toast.current.clear();
                toast.current.show({severity:'success', summary: 'Pay succeeded', detail:'your payment has been accepted', life: 3000});
                setTimeout(() => {
                    setShoppingList(shoppingList?.filter(element => element.id !== id_order));
                }, 3000);
            })
        }
    };

    const handleGetShoppingCartData = async (document, cartName) => {
        await getShoppingCartData("shopping-cart", document).then(res => {
            let auxArray = [];
            setShoppingList([]);
            if (user?.type === 'photographer' && user?.checkedAccount) {
                if (res) {
                    Object.keys(res).map((element) => {
                        const findOrders = res[element]?.cart?.filter(order => order?.name_photograph?.toLowerCase()?.split(" ")?.join('-') === cartName);
                        if (findOrders.length > 0) {
                            auxArray.push({
                                ...res[element],
                                cart: findOrders,
                                id: element
                            })
                        }

                    })
                }
            } else if (user?.type === 'buyer') {
                if (res) {
                    Object.keys(res).map((element) => {
                        if (element.includes(cartName)) {
                            auxArray.push({
                                ...res[element],
                                id: element
                            })
                        }
                    })
                }
            } else if (user?.type === "admin") {
                if (res) {
                    Object.keys(res).map((element) => {
                        auxArray.push({
                            ...res[element],
                            id: element
                        })
                    })
                }
            }
            setShoppingList(auxArray);
        })
    }

    const handleFinishOrder = async (order, element) => {
        if (order && element) {
            await setShoppingCartData("shopping-cart", "shopping-cart-old", 
            {
                [element.id]: {
                    address: element.address,
                    calendar: element.calendar,
                    isPaid: element.isPaid,
                    location: element.location,
                    cart: element?.cart?.map(item => {
                        if (item?.name_package === order?.name_package) {
                            item.isFinished = true;
                            item.reason = "";
                            item.isRejected = false;
                        }

                        return item;
                    })
                }
            }, { merge: true })
            .then(async res => {
                showSuccess();
                setShoppingList([]);
            })
            .catch(error => {
                showError();
            })
        }
    }

    const handleOnRejectEvent = (order, element) => {
        setShowReasonDialog(true);
        setPackageRejected(order);
        setOrderRejected(element);
    }

    const handleRejectOrder = async (order, element, reason) => {
        if (order && element) {
            await setShoppingCartData("shopping-cart", "shopping-cart-old", 
            {
                [element.id]: {
                    address: element.address,
                    calendar: element.calendar,
                    isPaid: false,
                    location: element.location,
                    cart: element?.cart?.map(item => {
                        if (item?.name_package === order?.name_package) {
                            item.isRejected = true;
                            item.isFinished = false;
                            item.reason = reason;
                        }

                        return item;
                    })
                }
            }, { merge: true })
            .then(async res => {
                showSuccess();
                setShoppingList([]);
            })
            .catch(error => {
                showError();
            })
        }
    }

    useEffect(() => {
        if (userName && shoppingList.length === 0) {
            handleGetShoppingCartData("shopping-cart-old", userName);
        } else if (shoppingList.length === 0 && user?.type === "admin") {
            handleGetShoppingCartData("shopping-cart-old", "");
        }
    }, [shoppingList?.length]);

    return (
        <div className="grid p-3 m-0">
            <div className="col-12">
                <Toast ref={toast} />
            </div>
            <div className="col-12">
                <AddFeedbackDialog
                    visibleDialog={ showReasonDialog }
                    setVisibleDialog={ setShowReasonDialog }
                    onSave={ (reason) => handleRejectOrder(packageRejected, orderRejected, reason) }
                    isOrder={true}
                />
            </div>
            {
                shoppingList.length > 0 && (userName || user?.type === "admin") ? shoppingList?.sort((firstElement, secondElement) => {
                    if (firstElement?.id?.toLowerCase() < secondElement?.id?.toLowerCase())
                        return 1;
                    
                    if (firstElement?.id?.toLowerCase() > secondElement?.id?.toLowerCase())
                        return -1;
                    
                    return 0;
                }).map((element, index) => {
                    return (
                        <div key={"client-" + index} className="col-12">
                            <Accordion activeIndex={0}>
                                <AccordionTab header={
                                        element.id
                                    }>
                                    <DataViewTemplate
                                        data={element?.cart ?? []}
                                        layout="list"
                                        renderListItem={(data) => (<div className="col-12">
                                            <div className="flex flex-column align-items-center p-3 w-full md:flex-row">
                                                {data?.image && <img className="md:w-11rem w-9 shadow-2 md:my-0 md:mr-5 mr-0 my-5 w-full h-7rem" src={data?.image} alt={data.name_package} />}
                                                <div className="text-center md:text-left md:flex-1">
                                                    <div className="text-2xl font-bold">{data?.name_package}</div>
                                                    <div className="mb-3">{data?.name_photograph}</div>
                                                    <div className="flex flex-row align-items-center">
                                                        <i className="pi pi-tag vertical-align-middle mr-2"></i>
                                                        <span className="vertical-align-middle font-semibold">{data?.category}</span>
                                                    </div>
                                                    {data?.reason !== '' && 
                                                    <>
                                                        <h4>Reason of rejecting</h4>
                                                        <p>{data?.reason}</p>
                                                    </>
                                                    }
                                                </div>
                                                <div className="flex md:flex-column mt-5 justify-content-between align-items-center md:w-auto w-full">
                                                    <span className="align-self-center text-2xl font-semibold mb-2 md:align-self-end">Price</span>
                                                    <span className="align-self-center text-2xl font-semibold mb-2 md:align-self-end">${data?.price}</span>
                                                    {user?.type === 'photographer' ?
                                                        <div className="grid">
                                                            <div className="col-12 md:col-6 lg:col-6">
                                                                <Button label='Accept' className="btn-fill" disabled={data?.isFinished || data?.isRejected} 
                                                                    onClick={() => !data?.isRejected ? handleFinishOrder(data, element) : showWarn('Order is declined, you can\'t accept the order anymore')} />
                                                            </div>
                                                            <div className="col-12 md:col-6 lg:col-6">
                                                                <Button label='Decline' className="btn-white" disabled={data?.isFinished || data?.isRejected} 
                                                                    onClick={() => !data?.isFinished ? handleOnRejectEvent(data, element) : showWarn('Order is accepted, you can\'t decline the order anymore')} />
                                                            </div>
                                                            
                                                        </div>
                                                    :
                                                        <div className="grid">
                                                            <div className="col-12 md:col-6 lg:col-6">
                                                                <Chip label="Accepted" className={`${data?.isFinished && !data?.isRejected ? "bg-green-500" : "surface-200 text-400"}`} />
                                                            </div>
                                                            <div className="col-12 md:col-6 lg:col-6">
                                                                <Chip label="Declined" className={`${!data?.isFinished && data?.isRejected ? "bg-red-400" : "surface-200 text-400"}`} />
                                                            </div>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </div>)}
                                        rows={9}
                                    />
                                    <h2>Location</h2>
                                    <h4>{element?.location ?? 'No info'}</h4>
                                    <h2>Address</h2>
                                    <h4>{element?.address ?? 'No info'}</h4>
                                    <h2>Meeting time</h2>
                                    <h4>{element?.calendar ?? 'No info'}</h4>
                                    <h2>Payment Status</h2>
                                    <Chip label={element?.isPaid === false && element?.cart?.filter(pack => pack?.isRejected)?.length > 0 ? "Money refund" :
                                    element?.isPaid ?
                                        "Paid" :
                                        "Not Paid"} 
                                    className={`${element?.isPaid ?
                                             "bg-green-500" : 
                                             "bg-red-400"}`} />
                                    <h2>Order Status</h2>
                                    <Chip 
                                    label={
                                        element?.cart?.filter(item => item.isRejected).length > 0 ? "Declined" : 
                                        element?.cart?.filter(item => item.isFinished).length > 0 ? "Accepted" : 
                                        "Need Action"
                                    } 
                                    className={`${element?.cart?.filter(item => item.isRejected).length > 0 ?
                                        "bg-red-400" :
                                        element?.cart?.filter(item => item.isFinished).length > 0 ?
                                        "bg-green-500":
                                        "surface-300 text-500"}`} 
                                    />
                                    {
                                        user?.type === 'admin' &&
                                        <div className="grid">
                                            <div className="col-12 md:col-12 lg:col-12">
                                                <Button label="Delete Order" icon="pi pi-trash" className="btn-fill mt-4" onClick={() => handleDeleteOrder(element?.id)} />
                                            </div>
                                        </div>
                                    }
                                </AccordionTab>
                            </Accordion>
                        </div>
                    )
                }) : <div className="col-12"><p className="text-center">No orders found</p></div>
            }
        </div>
    )
}

export default Orders;