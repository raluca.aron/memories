import '../styles/FourOFour.css';
import shadow from "../images/shadow.png";

function FourOFour() {
  return (
    <div className="FourOFour">
        <div className='text-center'>
          <h1 className='text-8xl m-0'>404</h1>
          <h2 className='text-2xl m-0'>Page not found</h2>
          <img src={shadow} alt="shadow" className='w-13rem mt-6' />
        </div>
    </div>
  );
}

export default FourOFour;
