import { useContext, useEffect, useState } from 'react';
import DataViewTemplate from '../components/DataViewTemplate';
import { getAlbumsData } from '../services/AlbumOperations';
import { listenerChangeAlbumsData } from '../services/ListenerData';
import { UserContext } from '../contexts/UserContext';
import { getCategoriesData } from '../services/CategoryOperations';
import FilterAlbums from '../components/FilterAlbums';

function Home() {

  const [layoutDataView, setLayoutDataView] = useState("grid");
 
  const [selectedOption, setSelectedOption] = useState(null);

  const [selectedPhotograph, setSelectedPhotograph] = useState(null);

  const [selectedLocation, setSelectedLocation] = useState(null);

  const [ratingNumber, setRatingNumber] = useState(0);

  const {userData: {user}} = useContext(UserContext);

  const [categoryOptions, setCategoryOptions] = useState([]);

  const [photographerList, setPhotographerList] = useState([]);

  const handleGetCategoriesData = async () => {
      await getCategoriesData("category").then(res => {
        const auxArray = []
        res?.forEach(element => {
            auxArray.push({
              code: element.id,
              name: element.data().name
            })
        });

        setCategoryOptions(auxArray);
    })
  }

  const handleGetPhotographsData = async () => {
    if (user) {
        await getAlbumsData("users").then(res => {
          let auxArray = []
          res?.forEach(element => {
            if (user?.type !== "photographer" && element.id !== user?.email) {
              element.data()?.gallery_photo?.forEach(cat => {
                auxArray.push({
                  email: element.id,
                  categories: cat.category,
                  name: element.data().firstName + " " + element.data().lastName,
                  price: element.data().hour_rate,
                  type: element.data().type,
                  image: element.data().image,
                  location: element.data().location,
                  checkedAccount: element.data().checkedAccount,
                  rating: element.data()?.rating?.map((element) => element.stars).reduce((accumulator, currentValue) => +accumulator + +currentValue, 0) / element.data()?.rating?.length
                })
              })
            }
          });
          setPhotographerList(auxArray);
      })
    }
  }

  useEffect(() => {
    listenerChangeAlbumsData("users", setPhotographerList)
    .then(res => {
      if (res)
        setPhotographerList([]);
    })
    .catch(error => {

    });
  }, [])

  useEffect(() => {
    handleGetCategoriesData()
  }, []);

  useEffect(() => {
    handleGetPhotographsData();
  }, [photographerList.length]);

  return (
    <div className="Home">
      <div className="grid w-full p-2">
        <div className="col-12">
          <FilterAlbums
            selectedCategory={selectedOption}
            setSelectedCategory={setSelectedOption}
            categoryOptions={categoryOptions}
            selectedPhotograph={selectedPhotograph}
            setSelectedPhotograph={setSelectedPhotograph}
            photographerOptions={[...new Set(photographerList?.map(element => (element && element?.name)) ?? [])]?.map(element => ({name: element, code: element?.toLowerCase()}))}
            selectedLocation={selectedLocation}
            setSelectedLocation={setSelectedLocation}
            locationOptions={[...new Set(photographerList?.map(element => (element && element?.location)) ?? [])]?.map(element => ({name: element, code: element?.toLowerCase()}))}
            ratingNumber={ratingNumber}
            setRatingNumber={setRatingNumber}
          />
        </div>
        
        <div className="col-12">
          <DataViewTemplate
            data={photographerList
              .filter(element => element?.checkedAccount && element)
              .filter(element => selectedOption ? element?.categories === selectedOption?.code && element : element)
              .filter(element => ratingNumber !== 0 ? parseInt(element?.rating) === ratingNumber && element : element)
              .filter(element => selectedPhotograph ? 
                element?.name?.toLowerCase() === selectedPhotograph?.name?.toLowerCase() && element : 
                element
              )
              .filter(element => selectedLocation ? 
                element?.location?.toLowerCase() === selectedLocation?.name?.toLowerCase() && element : 
                element
              )
            }
            layout={layoutDataView}
            setLayout={setLayoutDataView}
            rows={9}
          />
        </div>
      </div>
    </div>
  );
}

export default Home;
