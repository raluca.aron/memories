import { getUserData, setUserData } from '../services/UserOperations';
import { Password } from 'primereact/password';
import '../styles/Login.css';
import { InputText } from 'primereact/inputtext';
import { useContext, useRef, useState } from 'react';
import { Button } from 'primereact/button';
import { RoutingContext } from '../contexts/RoutingContext';
import { UserContext } from '../contexts/UserContext';
import { Link } from 'react-router-dom';
import { Toast } from 'primereact/toast';
import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';
import { TOAST_TIME } from '../constants/Functions';

function Login() {

  const [email, setEmail] = useState("");

  const [password, setPassword] = useState("");

  const [isSubmitted, setIsSubmitted] = useState(false);

  const { history } = useContext(RoutingContext);

  const { userData: {setUser} } = useContext(UserContext);

  const auth = getAuth();

  const toast = useRef(null);

  const showWarn = (message, summary) => {
    toast.current.clear();

    toast.current.show({
        severity:'warn', 
        summary: summary ? summary : 'The account doesn\'t exist', 
        detail: message ? message : 'There is no account registered with this email. Please register.', 
        life: TOAST_TIME
    });
  }

  const showError = () => {
    toast.current.clear();
    
    toast.current.show({
        severity:'error', 
        summary: 'Login failed', 
        detail:'The password isn\'t correct', 
        life: TOAST_TIME
    });
  }

  const resetSubmittedStatus = () => {
    setTimeout(() => {
        setIsSubmitted(false);
    }, TOAST_TIME);
  }

  const handleLogin = () => {
    setIsSubmitted(true);
    if (email !== "" && password !== "" && email.match(/(@[a-zA-Z]+.[a-zA=Z]+)$/gm)) {
        signInWithEmailAndPassword(auth, email, password)
        .then(async (userCredential) => {
            await setUserData("users", email, {
                isLogged: true
            }, { merge: true }).then(async res => {
                if (res) {
                    getUserData("users", userCredential.user.email).then(res => {
                        res.email = email;
                        setUser({
                            ...res
                        });
                        if (res?.firstLogin) {
                            history('/profile');
                        } else {
                            if (res?.type === "photographer") {
                                if (res?.checkedAccount === true)
                                    history(`/orders/${res?.firstName?.toLowerCase() + '-' + res?.lastName?.toLowerCase()}`)
                                else
                                    history("/profile");
                            } else 
                                history('/home');
                        }
                    })
                }
            })
        })
        .catch((error) => {
            if (error.code === "auth/user-not-found") {
                showWarn();
                setEmail("");
                setPassword("");
            } else if (error.code === "auth/wrong-password") {
                showError();
                setPassword("");
            }
        });

    } else if (email === "" && password === "") {
        showWarn("You need to provide an valid credentials", "Invalid data");
    } else if (email === "") {
        showWarn("You need to provide an email", "Invalid email");
    } else if (!email.match(/((\@)[a-zA-Z]+(\.)[a-zA=Z]+)$/gm)) {
        showWarn("You need to provide a valid email", "Invalid email");
        setEmail("");
    } else if (password === "") {
        showWarn("You need to provide a password", "Invalid Password");
    }

    resetSubmittedStatus();
  }


  return (
    <div className="Login align-center">
        <div className='form-login align-center shadow-6'>
            <div className='align-left'>
                <h3>Email</h3>
                <span className="p-input-icon-left">
                    <i className="pi pi-user" />
                    <InputText className={isSubmitted && email === "" ? 'p-invalid' : ""} value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Email" />
                </span>
            </div>
            <div className='align-left margin-bottom-between'>
                <h3>Password</h3>
                <Password 
                    value={password}
                    className={isSubmitted && password === "" ? 'p-invalid' : ""}
                    placeholder='Password'
                    onChange={(e) => setPassword(e.target.value)} 
                    feedback={false}
                    toggleMask />
            </div>
            <div className='align-center margin-bottom-between'>
                <Button label="Log In" className="btn-white" onClick={() => handleLogin()} />
            </div>
            <div className='align-center margin-bottom-between'>
                <p className='margin-zero'>Not a member?</p>
                <Link to='/register' className='text-white no-underline mt-3'>Sign up now</Link>
            </div>
        </div>
        <Toast ref={toast} />
    </div>
  );
}

export default Login;
