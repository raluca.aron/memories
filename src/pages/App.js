import '../styles/App.css';
import Login from './Login';
import {
  Routes,
  Route
} from "react-router-dom";
import ProtectedRoute from '../components/ProtectedRoute';
import FourOFour from './FourOfFour';
import Home from './Home';
import Register from './Register';
import Profile from './Profile';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { getUserData } from '../services/UserOperations';
import { useContext, useEffect } from 'react';
import { UserContext } from '../contexts/UserContext';
import { RoutingContext } from '../contexts/RoutingContext';
import Navbar from '../components/Navbar';
import ViewPhotographer from './ViewPhotographer';
import ShoppingCart from './ShoppingCart';
import Orders from './Orders';
import Favorites from './Favorites';
import ChatRoom from './ChatRoom';
import Users from './Users';
import Categories from './Categories';

function App() {
  
  const { userData: { setUser } } = useContext(UserContext);

  const { history, location } = useContext(RoutingContext);
  
  const auth = getAuth();

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
          getUserData("users", user.email)
          .then(res => {
              res.email = user.email;
              setUser({
                  ...res
              });
              if (res?.firstLogin) {
                  history('/profile');
              } else {
                  if (location.pathname === "/" && res?.type !== "photographer")
                    history('/home');
                  else
                    history(location.pathname);
              }
          });
      } else {
        setUser(null);
      }
    });
  }, [])

  return (
    <div className="App">
        {auth.currentUser && <Navbar labelAvatar={auth?.currentUser?.email[0]?.toUpperCase()} />}
        <Routes>
          <Route exact path='/home' element={<ProtectedRoute path='/home'><Home /></ProtectedRoute>}/>
          <Route exact path='/profile' element={<ProtectedRoute path='/profile'><Profile /></ProtectedRoute>}/>
          <Route exact path='/view-photographer/:email' element={<ProtectedRoute path='/view-photographer/:email'><ViewPhotographer /></ProtectedRoute>}/>
          <Route exact path='/shopping-cart/:cartName' element={<ProtectedRoute path='/shopping-cart/:cartName'><ShoppingCart /></ProtectedRoute>}/>
          <Route exact path='/orders/:userName' element={<ProtectedRoute path='/orders/:userName'><Orders /></ProtectedRoute>}/>
          <Route exact path='/favorites/:userName' element={<ProtectedRoute path='/favorites/:userName'><Favorites /></ProtectedRoute>}/>
          <Route exact path='/chatroom/:nameConversation' element={<ProtectedRoute path='/chatroom/:nameConversation'><ChatRoom /></ProtectedRoute>}/>
          <Route exact path='/chatroom' element={<ProtectedRoute path='/chatroom'><ChatRoom /></ProtectedRoute>}/>
          <Route exact path='/users' element={<ProtectedRoute path='/users'><Users /></ProtectedRoute>}/>
          <Route exact path='/category' element={<ProtectedRoute path='/category'><Categories /></ProtectedRoute>}/>
          <Route exact path='/orders' element={<ProtectedRoute path='/orders'><Orders /></ProtectedRoute>}/>
          <Route exact path='/fourofour' element={<FourOFour />} />
          <Route exact path='/' element={<Login />} />
          <Route exact path='/register' element={<Register />} />
        </Routes>
    </div>
  );
}

export default App;
