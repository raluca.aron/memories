import { Avatar } from 'primereact/avatar';
import { Button } from 'primereact/button';
import { Chip } from 'primereact/chip';
import { Galleria } from 'primereact/galleria';
import { Image } from 'primereact/image';
import { InputNumber } from 'primereact/inputnumber';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { Rating } from 'primereact/rating';
import { Toast } from 'primereact/toast';
import { useContext, useEffect, useRef, useState } from 'react';
import AddAlbumDialog from '../components/AddAlbumDialog';
import AddPackageDialog from '../components/AddPackageDialog';
import AddPhotoDialog from '../components/AddPhotoDialog';
import DataViewTemplate from '../components/DataViewTemplate';
import { RoutingContext } from '../contexts/RoutingContext';
import { UserContext } from '../contexts/UserContext';
import { getCategoriesData } from '../services/CategoryOperations';
import { getUserData, setUserData } from '../services/UserOperations';
import '../styles/Profile.css';

function Profile() {
 const { userData: {user, setUser} } = useContext(UserContext);

 const { history } = useContext(RoutingContext);

 const [editMode, setEditMode] = useState(false);

 const [email, setEmail] = useState('');

 const [firstName, setFirstName] = useState('');

 const [lastName, setLastName] = useState('');

 const [location, setLocation] = useState('');

 const [socialMedia, setSocialMedia] = useState("");

 const [price, setPrice] = useState(0);

 const [description, setDescription] = useState('');

 const [packages, setPackages] = useState([]);

 const [galleryPhoto, setGalleryPhoto] = useState([]);

 const [showAlbum, setShowAlbum] = useState([]);

 const [showAlbumCarousel, setShowAlbumCarousel] = useState(false);

 const [thumbnail, setThumbnail] = useState("");

 const [showAddAlbumDialog, setShowAddAlbumDialog] = useState(false);

 const [showAddProfileDialog, setShowAddProfileDialog] = useState(false);

 const [showAddPackageDialog, setShowAddPackageDialog] = useState(false);

 const [categoryOptions, setCategoryOptions] = useState([]);

 const toast = useRef(null);

const handleGetPackagesData = async (id_photograph) => {
    await getUserData("packages", id_photograph).then(res => {
        setPackages(res?.packages ?? []);
    })
}

const handleGetCategoriesData = async () => {
    await getCategoriesData("category").then(res => {
      const auxArray = []
      res?.forEach(element => {
          auxArray.push({
            code: element.id,
            name: element.data().name
          })
      });
      setCategoryOptions(auxArray);
  })
}

 useEffect(() => {
    if (user) {
        setEmail(user.email ?? '');
        setFirstName(user.firstName ?? '');
        setLastName(user.lastName ?? '');
        setLocation(user.location ?? '');
        if (user?.type === "photographer") {
            setPrice(user?.hour_rate ?? 0);
            setDescription(user?.description ?? '');
            setThumbnail(user?.image ?? "");
            setGalleryPhoto(user?.gallery_photo ?? []);
            setSocialMedia(user?.socialMedia ?? "");
            handleGetPackagesData(user?.email);
            handleGetCategoriesData();
        }
    }
 }, []);

 const handleRemovePhoto = async () => {
    setThumbnail("");
 };

 const handleUpdateProfile = async () => {
    let bodyData = {}

    if (firstName !== '') {
        bodyData.firstName = firstName;
    }

    if (lastName !== '') {
        bodyData.lastName = lastName;
    }

    if (location !== '') {
        bodyData.location = location;
    }

    if (user?.type === "photographer") {
        if (price !== 0) {
            bodyData.hour_rate = price;
        }
        if (description !== "") {
            bodyData.description = description;
        }
        if (thumbnail !== "") {
            bodyData.image = thumbnail;
        }
        if (galleryPhoto?.length > 0) {
            bodyData.gallery_photo = galleryPhoto;
        }
        if (socialMedia !== "") {
            bodyData.socialMedia = socialMedia;
        }
    }

    if (Object.keys(bodyData).length > 0) {
            await setUserData("users", user.email, bodyData, { merge: true })
            .then(res => {
                if (!res?.hasOwnProperty("code")) {  
                    setUser(res);
                    showSuccess();
                }
            })
    } else {
        showWarn("All fields are required");
    }
    if (user?.type === "photographer") {
        if (packages?.length > 0) {
            await setUserData("packages", user?.email, {
                packages: packages
            }, { merge: true }).then(res => {
                if (!res?.hasOwnProperty("code")) {
                    showSuccess();
                }
            });
        } else {
            showWarn("Need at least 1 package offer");
        }
    }
    setEditMode(false);
 }

 const handleNextStep = () => {
    if ((firstName !== "" && lastName !== "" && location !== "" && (user?.type === "buyer" || user?.type === "admin"))
    || (firstName !== "" && lastName !== "" && location !== "" && socialMedia !== "" && user?.type === "photographer")) {
        setUserData("users", user.email, {firstLogin: false}, { merge: true })
        .then(res => {
            if (res) {  
                setUser({
                    ...user,
                    firstLogin: false
                })
                if (user?.type !== "photographer")
                    history("/home");
                else
                    history(`/orders/${user?.firstName?.toLowerCase() + "-" + user?.lastName?.toLowerCase()}`);
            }
        })
    }

 }

 const resetExitEditMode = () => {
    setEmail(user?.email ?? '');
    setFirstName(user?.firstName ?? '');
    setLastName(user?.lastName ?? '');
    setLocation(user?.location ?? '');
    if (user?.type === "photographer") {
        setPrice(user?.hour_rate ?? 0);
        setDescription(user?.description ?? '');
        setThumbnail(user?.image ?? "");
        setGalleryPhoto(user?.gallery_photo ?? []);
        setSocialMedia(user?.socialMedia ?? "");
    }
    setEditMode(false);
}

 const showSuccess = () => {
    if (toast)
    toast.current.show({severity:'success', summary: 'Operation succed', detail:'Data has been modified', life: 3000});
};

const showWarn = (message) => {
    if (toast)
        toast.current.show({severity:'warn', summary: 'Warning', detail: message, life: 3000});
};

const handleDeletePackage = async (name_package) => {
    if (name_package) {
        setPackages(packages.filter(element => element.name !== name_package));
    }
}

const itemGridGalleriaImageTemplate = (item) => {
    return <img src={item.src} alt={item.alt} className="w-9 my-5 shadow-3 w-full h-15rem" />;
};

 const handleShowAlbum = (carousel) => {
    setShowAlbum(carousel);
    setShowAlbumCarousel(true);
 }

 const handleDeleteAlbum = (category) => {
    setGalleryPhoto(galleryPhoto?.filter(element => element.category !== category));
 }

 const handleSaveAdd = (data) => {
    setGalleryPhoto([...galleryPhoto, data]);
 }

 const handleSaveAddPackage = (data) => {
    setPackages([...packages, data]);
 }

 const handleExitMode = () => {
    setEditMode(false);
 }
 
  return (
    <>
    {
        user?.type !== "photographer" ?
        <div className="Settings grid p-2 m-0">
            <div className="col-12 md:col-2 lg:col-2 flex flex-column justify-content-start align-items-center border-right-1 border-200">
                <Avatar
                    label={lastName !== "" ? lastName?.[0]?.toUpperCase() : "U"}
                    size="xlarge"
                    shape="circle"
                    style={{ backgroundColor: "#8cbf7c", color: "#fff" }}
                />
                <h3>{firstName ?? ""} {lastName ?? ""}</h3>
                {!editMode && <Button label='Edit' className='btn-fill' onClick={() => setEditMode(true)} />}
            </div>
            <div className='col-12 md:col-10 lg:col-10'>
                <div className='grid p-2'>
                    <div className='col-12'>
                        <h2>
                            User settings
                        </h2>
                    </div>
                    <div className='col-12 md:col-6 lg:col-6'>
                        <h3>Email</h3>
                        <p>
                            {email}
                        </p>
                    </div>
                    <div className='col-12 md:col-6 lg:col-6'>
                        <h3>First Name</h3>
                        <span className="p-input-icon-left w-10">
                            <i className="pi pi-user" />
                            <InputText 
                                value={firstName}
                                disabled={ !editMode }
                                onChange={(e) => setFirstName(e.target.value)} 
                                placeholder="First Name"
                                className='w-full'
                            />
                        </span>
                    </div>
                    <div className='col-12 md:col-6 lg:col-6'>
                        <h3>Last Name</h3>
                        <span className="p-input-icon-left w-10">
                            <i className="pi pi-user" />
                            <InputText 
                                value={lastName}
                                disabled={ !editMode }
                                onChange={(e) => setLastName(e.target.value)} 
                                placeholder="Last Name"
                                className='w-full'
                            />
                        </span>
                    </div>
                    <div className='col-12 md:col-6 lg:col-6'>
                        <h3>Location</h3>
                        <span className="p-input-icon-left w-10">
                            <i className="pi pi-globe" />
                            <InputText 
                                value={location}
                                disabled={ !editMode }
                                onChange={(e) => setLocation(e.target.value)} 
                                placeholder="Location"
                                className='w-full'
                            />
                        </span>
                    </div>
                    <div className='col-12'>
                        {
                            editMode &&
                            <>
                                <Button label="Update Profile" className="btn-fill" onClick={() => handleUpdateProfile()}/>
                                <Button label="Cancel" className="btn-white ml-4" onClick={() => handleExitMode()}/>
                            </>
                                
                        }
                        {
                            user?.firstLogin &&
                            <Button label='Next Step' className="btn-fill ml-4" disabled={
                                user?.firstName === "" ||  user?.lastName === "" ||  user?.location === ""
                            } onClick={() => handleNextStep()} />
                        }
                    </div>
                </div>
            </div>

        </div>
        :
        <div className="grid p-3 m-0">
            <div className="col-12 md:col-12 lg:col-12 flex align-items-center justify-content-center">
                {
                    editMode ?
                        
                            thumbnail === "" ?
                                <>
                                    <Button label="Add profile photo" className='btn-fill' onClick={() => setShowAddProfileDialog(true)} />
                                    <AddPhotoDialog
                                        visibleDialog={ showAddProfileDialog }
                                        setVisibleDialog={ setShowAddProfileDialog }
                                        onUpload={(photo) => setThumbnail(photo)}
                                    />
                                </>
                            :                        
                            <div className="col-1" style={{position: 'relative', width: 'auto'}}>
                                {thumbnail !== "" && <Button icon="pi pi-times" className="btn-fill" rounded="true" outlined="true" severity="danger" aria-label="Cancel" 
                                style={{position: 'absolute', top: '-5%', left: '100%', transform: 'translate(-70%, 30%)', zIndex: '1'}} 
                                onClick={() => {
                                    handleRemovePhoto()
                                }}/>}
                                <Image 
                                    src={thumbnail}
                                    alt="Image"
                                    width="500"
                                    style={{ cursor: 'pointer', width: '300px', height: '250px' }} 
                                />
                            </div>
                        

                    :
                        <Image src={thumbnail ?? ""} alt="Image" width="500" preview />
                }
            </div>
            <div className="col-12 md:col-12 lg:col-12">
                {
                    editMode ?
                        <Button label={'Exit edit mode'} className="btn-fill" onClick={() => resetExitEditMode()}/>
                    :
                        <Button label={'Edit mode'} className="btn-fill" disabled={!user?.checkedAccount} onClick={() => setEditMode(true)}/>
                }
                
            </div>
            {
                !user?.checkedAccount &&
                <div className="col-12 md:col-12 lg:col-12">
                    <Chip
                    label={
                        "Account disabled"
                    } 
                    className={"surface-300 text-500"} 
                    />
                </div>
            } 
            <div className="col-12 md:col-6 lg:col-6">
                <h3>First Name</h3>
                {
                    editMode ? 
                        <span className="p-input-icon-left w-10">
                            <i className="pi pi-user" />
                            <InputText 
                                value={firstName} 
                                onChange={(e) => setFirstName(e.target.value)} 
                                placeholder="First Name"
                                className='w-full'
                            />
                        </span>
                    :
                        <p>
                            {firstName}
                        </p>
                }
                <h3>Last Name</h3>
                {
                    editMode ? 
                        <span className="p-input-icon-left w-10">
                            <i className="pi pi-user" />
                            <InputText 
                                value={lastName} 
                                onChange={(e) => setLastName(e.target.value)} 
                                placeholder="Last Name"
                                className='w-full'
                            />
                        </span>
                    :
                        <p>
                            {lastName}
                        </p>
                }
                <h3>Location</h3>
                {
                    editMode ? 
                        <span className="p-input-icon-left w-10">
                            <i className="pi pi-globe" />
                            <InputText 
                                value={location} 
                                onChange={(e) => setLocation(e.target.value)} 
                                placeholder="Location"
                                className='w-full'
                            />
                        </span>
                    :
                        <p>
                            {location}
                        </p>
                }
                <h3>Social media</h3>
                {
                    editMode ? 
                        <span className="p-input-icon-left w-10">
                            <i className="pi pi-link" />
                            <InputText
                                value={socialMedia} 
                                onChange={(e) => setSocialMedia(e.target.value)} 
                                placeholder="Social media link"
                                className='w-full'    
                            />
                        </span>
                    :
                        <p>
                            {socialMedia}
                        </p>
                }
            </div>
            <div className="col-12 md:col-6 lg:col-6 flex flex-column align-items-center justify-content-around">
                <Rating value={user?.rating ?? 0} readOnly={true} cancel={false} />
                <div>
                    <h3>Hour rate</h3>
                    {
                        editMode ?
                            <InputNumber value={price} onValueChange={(e) => setPrice(e.value)} />
                        :
                        <p className='text-xl font-bold'>${price ?? '0.00'}</p>
                    } 
                </div>
                
            </div>
            <div className="col-12 md:col-12 lg:col-12">
                <h3>Description</h3>
                {
                    editMode ?
                        <InputTextarea
                            className="mt-2 w-full"
                            value={description}
                            onChange={(e) => setDescription(e.target.value)} 
                            rows={15} 
                        />
                    :
                    <p className='white-space-normal'>{description ?? 'No info'}</p>
                }
                
            </div>
            <div className="col-12 card">
                <h3>Gallery</h3>
                {
                    editMode &&
                    <>
                        <Button label="Add Album" icon="pi pi-plus" className='btn-fill mb-4' onClick={ () => setShowAddAlbumDialog(true) } />
                        <AddAlbumDialog 
                            visibleDialog={showAddAlbumDialog}
                            setVisibleDialog={setShowAddAlbumDialog}
                            categoryOptions={ categoryOptions }
                            onSave={ handleSaveAdd }
                        />
                    </>
                }
                <div className="flex align-items-center justify-content-center">
                        <DataViewTemplate
                            data={galleryPhoto ?? []}
                            lazy={true}
                            layout="grid"
                            renderGridItem={(data) => (
                                <div className="col-12 md:col-4">
                                <div className="m-2 border-1 border-round-sm p-2 surface-border card">
                                    <div className="flex align-items-center justify-content-between">
                                        <div>
                                            <i className="pi pi-tag vertical-align-middle mr-2"></i>
                                            <span className="vertical-align-middle font-semibold">{data?.category}</span>
                                        </div>
                                    </div>
                                    <div className="text-center">
                                        <Galleria 
                                            value={(data?.carousel ?? []).map((element) => ({src: element?.image, alt: element?.name}))} 
                                            showThumbnails={false} 
                                            showIndicators
                                            style={{ maxWidth: '640px' }} 
                                            showIndicatorsOnItem={null} 
                                            indicatorsPosition={'bottom'} 
                                            item={itemGridGalleriaImageTemplate} 
                                        />
                                    </div>
                                    <div className="flex align-items-center justify-content-between">
                                    <Button icon="pi pi-camera" className="btn-fill mb-2" label="View Album" disabled={!user?.checkedAccount} onClick={ () => handleShowAlbum(data?.carousel) }></Button>
                                    {editMode && <Button className="btn-fill mb-2" icon="pi pi-trash" label="Delete Album" onClick={() => handleDeleteAlbum(data?.category)}></Button>}
                                    </div>
                                </div>
                            </div>
                            )}
                            rows={9}
                        />
                </div>
                {
                    showAlbumCarousel &&
                    <div className="fixed top-0 bottom-0 left-0 right-0 z-4 bg-black-alpha-60 flex flex-column align-items-center justify-content-center">
                        <Galleria
                            value={(showAlbum ?? [])?.map(element => ({src: element?.image, alt: element?.name}))} 
                            showItemNavigators={true}
                            className="z-5"
                            responsiveOptions={[
                                {
                                    breakpoint: '1500px',
                                    numVisible: 5
                                },
                                {
                                    breakpoint: '1024px',
                                    numVisible: 3
                                },
                                {
                                    breakpoint: '768px',
                                    numVisible: 2
                                },
                                {
                                    breakpoint: '560px',
                                    numVisible: 1
                                }
                            ]}
                            style={{ maxWidth: '624px' }}
                            circular
                            item={(item) => <img src={item.src} alt={item.alt} style={{ width: '100%', height: '350px' }} />}
                            thumbnail={(item) => <img src={item.src} alt={item.alt} style={{ width: '225px', height: '150px', padding: '0.5rem' }} />} 
                        />  
                        <Button className="btn-fill mt-4" label="Cancel album" onClick={() => setShowAlbumCarousel(false)} />
                    </div>
                }
            </div>
            <div className="col-12">
                <h3>Packages</h3>
                {
                    editMode &&
                    <>
                        <Button label="Add Package" icon="pi pi-plus" className='btn-fill mb-4' onClick={ () => setShowAddPackageDialog(true) } />
                        <AddPackageDialog 
                            visibleDialog={showAddPackageDialog}
                            setVisibleDialog={setShowAddPackageDialog}
                            categoryOptions={ categoryOptions }
                            onSave={ handleSaveAddPackage }
                        />
                    </>
                }
                <DataViewTemplate
                data={packages ?? []}
                layout="list"
                renderListItem={(data) => (<div className="col-12">
                    <div className="flex flex-column align-items-center p-3 w-full md:flex-row">
                        <div className="text-center md:text-left md:flex-1">
                            <div className="text-2xl font-bold">{data?.name}</div>
                            <div className="mb-3 white-space-normal">{data?.description}</div>
                            <i className="pi pi-tag mr-2"></i>
                            <span className="font-semibold">{data?.category}</span>
                        </div>
                        <div className="flex md:flex-column mt-5 justify-content-between align-items-center md:w-auto w-full">
                            <span className="align-self-center text-2xl font-semibold mb-2 md:align-self-end">${data?.price}</span>
                            {editMode && <Button label="Delete" icon="pi pi-trash" className='btn-fill' onClick={() => handleDeletePackage(data?.name)} />}
                        </div>
                    </div>
                </div>)}
                rows={9}
                />
            </div>
            {
                editMode &&
                <div className='col-12'>
                    <Button label="Save" className="btn-fill" onClick={() => handleUpdateProfile()}/>
                    <Button label="Cancel" className="btn-white ml-4" onClick={() => resetExitEditMode()}/>
                </div>
            }
            {
                user?.firstLogin &&
                <div className='col-12'>
                    <Button label='Next Step' className='btn-fill' disabled={
                        user?.firstName === "" || user?.lastName === "" || user?.location === "" || user?.image === "" ||
                        user?.description === "" || user?.gallery_photo.length === 0 || user?.hour_rate === 0 || packages.length === 0
                    } onClick={() => handleNextStep()} /> 
                </div>
            }
        </div>
    }
        <div className="col-12 md:col-12 lg:col-12">
            <Toast ref={toast} />
        </div>
    </>
  );
}

export default Profile;
