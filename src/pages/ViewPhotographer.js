import React, { useContext, useEffect, useRef, useState } from "react";
import { Image } from 'primereact/image';
import { getAlbumsData, setAlbumData } from "../services/AlbumOperations";
import { useParams } from "react-router-dom";
import { Rating } from "primereact/rating";
import { Button } from "primereact/button";
import { listenerChangeCollData } from "../services/ListenerData";
import { UserContext } from "../contexts/UserContext";
import { getShoppingCartData, setShoppingCartData } from "../services/ShoppingCartOperations";
import { getUserData, setUserData } from "../services/UserOperations";
import { Toast } from "primereact/toast";
import { RoutingContext } from "../contexts/RoutingContext";
import DataViewTemplate from "../components/DataViewTemplate";
import { Galleria } from "primereact/galleria";
import { Carousel } from 'primereact/carousel';
import AddFeedbackDialog from "../components/AddFeedbackDialog";

const ViewPhotographer = () => {

    const [photographer, setPhotographer] = useState(null);

    const [isInFav, setIsInFav] = useState(false);

    const [showAlbumCarousel, setShowAlbumCarousel] = useState(false);

    const [showAlbum, setShowAlbum] = useState([]);

    const [packages, setPackages] = useState([]);
    
    const { history } = useContext(RoutingContext);

    const {email} = useParams();

    const toast = useRef(null);

    const [showFeedbackDialog, setShowFeedbackDialog] = useState(false);

    const {userData: {user}} = useContext(UserContext);

    const [cart, setCart] = useState([]);

    const handleGetShoppingCartData = async (document, cartName) => {
        await getShoppingCartData("shopping-cart", document).then(res => {
            if (res?.hasOwnProperty(cartName) && email) {
                setCart(res[cartName]?.cart);
            }
        })
    }

    const handleGetFavData = async () => {
        if (user && photographer) {
            await getAlbumsData("favorites")
            .then(res => {
                if (res) {
                    res?.forEach(element => {
                        if (element?.id === user?.firstName?.toLowerCase() + "-" + user?.lastName?.toLowerCase()) {
                            const data = element?.data();
                            if (data?.photographers?.filter(element => element.email === photographer?.email).length > 0) {
                                setIsInFav(true);
                            }  
                        }
                    });  
                }
            });
        }
    }

    const handleGetPhotographerData = async (id_photograph) => {
        await getUserData("users", id_photograph).then(res => {
            if (res) {
                res.email = id_photograph;
                setPhotographer({
                    ...res
                });
            }
        })
    }

    const handleGetPackagesData = async (id_photograph) => {
        await getUserData("packages", id_photograph).then(res => {
            if (res) {
                setPackages(res?.packages ?? []);
            }
        })
    }

    useEffect(() => {
        listenerChangeCollData("users", email)
        .then(res => {
           if (res?.checkedAccount) {
            res.email = email;
            setPhotographer(res);
           }
        });
    })

    useEffect(() => {

        if (user)
            handleGetShoppingCartData("shopping-chart-new", 
                user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase());

        if (user && photographer?.checkedAccount) {
            handleGetFavData();
        }

        if (photographer?.checkedAccount) {
            handleGetPhotographerData(photographer?.email);
            handleGetPackagesData(photographer?.email);
        }

    }, [user?.email, photographer?.email, cart?.length]);

    const handleAddToCart = async (pack) => {
        if (user && photographer && email && pack) {
            await getShoppingCartData("shopping-cart", "shopping-chart-new").then(async res => {
                if (res?.hasOwnProperty(user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()) &&
                 email) {
                    const newCart = [...res[user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()].cart, {
                        category: pack?.category ?? '',
                        id_photograph: photographer?.email,
                        id_user: user?.email,
                        name_package: pack?.name,
                        name_photograph: photographer?.firstName + " " + photographer?.lastName,
                        price: pack?.price,
                        image: photographer?.image,
                    }];
                    await setShoppingCartData("shopping-cart", "shopping-chart-new", 
                    {
                        [user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()]: {
                            ...res[user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()],
                            cart: newCart
                        }
                    })
                    .then(res => {
                        showSuccess();
                        setCart([...cart, {
                            category: pack?.category ?? '',
                            id_photograph: photographer?.email,
                            id_user: user?.email,
                            name_package: pack?.name,
                            name_photograph: photographer?.firstName + " " + photographer?.lastName,
                            price: pack?.price,
                            image: photographer?.image,
                        }]);
                    })
                    .catch(error => {
                        console.log(error);
                        showError();
                    })
                } else {
                    await setShoppingCartData("shopping-cart", "shopping-chart-new", {
                        [user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()]: {
                            isPaid: false,
                            address: '',
                            calendar: '',
                            location: photographer?.location,
                            cart: [
                                {
                                    category: pack?.category ?? '',
                                    id_photograph: photographer?.email,
                                    id_user: user?.email,
                                    name_package: pack?.name,
                                    name_photograph: photographer?.firstName + " " + photographer?.lastName,
                                    price: pack?.price,
                                    image: photographer?.image,
                                }
                            ]
                        }
                    }).then(res => {
                        if (res) {
                            showSuccess();
                        } else {
                            showError();
                        }
                    })
                }
            })
        }

    }

    const showSuccess = () => {
        if (toast)
            toast.current.show({severity:'success', summary: 'Operation succeeded', detail:'Data has been modified', life: 3000});
    };

    const showError = () => {
        if (toast)
            toast.current.show({severity:'error', summary: 'Operation failed', detail:'Data hasn\'t been modified', life: 3000});
    }

    const handleAddToFavorites = async () => {
        if (user && photographer) {
            await getAlbumsData("favorites")
            .then(async res => {
                if (res) {
                    const auxArray = []
                    res?.forEach(element => {
                        if (element?.id === user?.firstName?.toLowerCase() + "-" + user?.lastName?.toLowerCase()) {
                            const data = element?.data();
                            data?.photographers?.map(item => {
                                auxArray.push({
                                    ...item
                                });
                            })
                        }  
                    });
                    if (auxArray.filter(element => element.email === photographer?.email).length === 0) {
                        await setAlbumData("favorites", user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase(), {
                            photographers: [...auxArray, photographer]
                        }, { merge: true }).then(res => {
                            if (res) {
                                showSuccess();
                                setIsInFav(true);
                            }
                        });
                    } else {
                        setIsInFav(true);
                    }
                }
            });
        }

    }
    
    const handleShowCarousel = (carousel) => {
        setShowAlbum(carousel);
        setShowAlbumCarousel(true);
    }

    const itemGridGalleriaImageTemplate = (item) => {
        return <img src={item.src} alt={item.alt} className="w-11 my-5 shadow-3 h-15rem" />;
    };

    const productTemplate = (product) => {
        return (
            <div className="border-1 surface-border border-round m-2 text-center py-3 px-3">
                <div className="mb-3">
                    <Galleria 
                        value={(product?.carousel ?? []).map((element) => ({src: element?.image, alt: element?.name}))} 
                        showThumbnails={false} 
                        showIndicators
                        style={{ maxWidth: '640px' }} 
                        showIndicatorsOnItem={null} 
                        indicatorsPosition={'bottom'} 
                        item={itemGridGalleriaImageTemplate} 
                    />
                </div>
                <div>
                    <div>
                        <i className="pi pi-tag vertical-align-middle mr-2"></i>
                        <span className="vertical-align-middle font-semibold">{product?.category}</span>
                    </div>
                    <div className="mt-5 flex flex-wrap gap-2 justify-content-center">
                        <Button label="View Album" className="btn-fill" onClick={() => handleShowCarousel(product?.carousel) } />
                    </div>
                </div>
            </div>
        );
    };

    const disableProfilePhotograph = async () => {
        if (photographer && user) {
                await setUserData("users", photographer?.email, { checkedAccount: false }, { merge: true })
                .then(res => {
                    if (res) {  
                        setPhotographer(null);
                        showSuccess();
                    }
                }) 
        }
    }

    const handleSaveRating = async (stars) => {
        if (stars !== 0 && photographer && user) {
            await setUserData("users", photographer?.email, {rating: [...photographer?.rating, {
                email: user?.email,
                stars: stars
            }]}, { merge: true })
            .then(res => {
                if (res) {  
                    setPhotographer(res);
                    showSuccess();
                }
            }) 
        }
    }

    const handleStartChat = async () => {
        if (email && user) {
            await getUserData("messages", `${email}:${user?.email}`)
            .then(async res => {
                if (!res?.hasOwnProperty("customData")) {
                    if (!res) {
                        await setUserData("messages", `${email}:${user?.email}`, {
                            messages: []
                        }).then(res => {
                            if (!res?.hasOwnProperty("customData")) {
                                history(`/chatroom/${email}:${user?.email}`);
                            }
                        })
                    } else
                        history(`/chatroom/${email}:${user?.email}`);
                }
            })
        }
    }

    return (
        <div className="grid p-3 m-0">
            <div className="col-12">
                <AddFeedbackDialog
                    visibleDialog={ showFeedbackDialog }
                    setVisibleDialog={ setShowFeedbackDialog }
                    onSave={ handleSaveRating }
                />
            </div>
            <div className="col-12 md:col-12 lg:col-12">
                <Toast ref={toast} />
            </div>
            <div className="col-12 md:col-12 lg:col-12 flex align-items-center justify-content-center">
                <Image src={photographer?.image} alt="Photographer Profile" width="450" preview />
            </div>
            <div className="col-12 md:col-6 lg:col-6">
                <h2>Photographer name</h2>
                <p>{(photographer?.firstName ?? "No info") + " " + (photographer?.lastName ?? "") ?? 'No info'}</p>
                <h5>Location</h5>
                <p>{photographer?.location ?? 'No info'}</p>
            </div>
            <div className="col-12 md:col-6 lg:col-6 flex flex-column align-items-center justify-content-around">
                <Rating value={photographer?.rating?.map((element) => element.stars).reduce((accumulator, currentValue) => +accumulator + +currentValue, 0) / photographer?.rating?.length ?? 0} readOnly={true} cancel={false} />
                <div>
                    <h2>Hour rate</h2>
                    <h2>${photographer?.hour_rate ?? '0.00'}</h2>
                </div>
                {user?.type === 'buyer' && <Button label="Start chat"  className="btn-fill mb-4" onClick={() => handleStartChat()}/>}
                {user?.type === 'buyer' && <Button label="Favorites" className="btn-fill mb-4" disabled={isInFav} onClick={() => handleAddToFavorites()}/>}
                {user?.type === 'buyer' && <Button label="Rating" className="btn-fill" disabled={photographer?.rating?.filter((element) => element.email === user?.email).length > 0} onClick={() => setShowFeedbackDialog(true)}/>}
                {user?.type === 'admin' && <Button label="Disable Account" className="btn-fill" onClick={() => disableProfilePhotograph()}/>}
            </div>
            <div className="col-12 md:col-12 lg:col-12">
                <h4>Description</h4>
                <p className='white-space-normal'>{photographer?.description ?? 'No info'}</p>
            </div>
            <div className="col-12">
                <h4>Gallery</h4>
                <div className="flex align-items-center justify-content-center">
                    <Carousel value={photographer?.gallery_photo ?? []} numVisible={3} numScroll={3} responsiveOptions={[
                        {
                            breakpoint: '1199px',
                            numVisible: 1,
                            numScroll: 1
                        },
                        {
                            breakpoint: '991px',
                            numVisible: 2,
                            numScroll: 1
                        },
                        {
                            breakpoint: '767px',
                            numVisible: 1,
                            numScroll: 1
                        }
                    ]} itemTemplate={productTemplate} />
                </div>
                {
                    showAlbumCarousel &&
                    <div className="fixed top-0 bottom-0 left-0 right-0 z-4 bg-black-alpha-60 flex flex-column align-items-center justify-content-center">
                        <Galleria
                            value={(showAlbum ?? [])?.map(element => ({src: element?.image, alt: element?.name}))} 
                            showItemNavigators={true}
                            className="z-5"
                            responsiveOptions={[
                                {
                                    breakpoint: '1500px',
                                    numVisible: 5
                                },
                                {
                                    breakpoint: '1024px',
                                    numVisible: 3
                                },
                                {
                                    breakpoint: '768px',
                                    numVisible: 2
                                },
                                {
                                    breakpoint: '560px',
                                    numVisible: 1
                                }
                            ]}
                            style={{ maxWidth: '624px' }}
                            circular
                            item={(item) => <img src={item.src} alt={item.alt} style={{ width: '100%', height: '350px' }} />}
                            thumbnail={(item) => <img src={item.src} alt={item.alt} style={{ width: '225px', height: '150px', padding: '0.5rem' }} />} 
                        />  
                        <Button className="btn-fill mt-4" label="Cancel album" onClick={() => setShowAlbumCarousel(false)} />
                    </div>
                }
            </div>
            <div className="col-12">
                <h4>Packages</h4>
                <DataViewTemplate
                data={packages ?? []}
                layout="list"
                renderListItem={(data) => (<div className="col-12">
                    <div className="flex flex-column align-items-center p-3 w-full md:flex-row">
                        <div className="text-center md:text-left md:flex-1 w-full">
                            <p className="text-2xl font-bold">{data?.name}</p>
                            <p className="mb-3 white-space-normal">{data?.description}</p>
                            <i className="pi pi-tag vertical-align-middle mr-2"></i>
                            <span className="vertical-align-middle font-semibold">{data?.category}</span>
                        </div>
                        <div className="flex md:flex-column mt-5 justify-content-between align-items-center md:w-auto w-full">
                            <span className="align-self-center text-2xl font-semibold mb-2 md:align-self-end">${data?.price}</span>
                            {user?.type === "buyer" && <Button label="Buy package" className="btn-fill" disabled={cart?.filter(element => element.name_package === data?.name).length > 0} onClick={() => handleAddToCart(data)} />}
                        </div>
                    </div>
                </div>)}
                rows={9}
                />
            </div>
        </div>
    )
}

export default ViewPhotographer;