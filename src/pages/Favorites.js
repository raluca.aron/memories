import React, { useContext, useEffect, useRef, useState } from "react";
import { getAlbumsData, setAlbumData } from "../services/AlbumOperations";
import { useParams } from "react-router-dom";
import { Rating } from "primereact/rating";
import { Button } from "primereact/button";
import { listenerChangeAlbumsData } from "../services/ListenerData";
import DataViewTemplate from "../components/DataViewTemplate";
import { Toast } from "primereact/toast";
import { RoutingContext } from "../contexts/RoutingContext";

const Favorites = () => {
    
    const [favoritesList, setFavoritesList] = useState(null);

    const toast = useRef(null);

    const {userName} = useParams();
    
    const { history } = useContext(RoutingContext);

    const handleGetFavoritesAlbum = async (userName) => {
        await getAlbumsData("favorites").then(res => {
            const auxArray = []
            res?.forEach(element => {
                if (element?.id === userName) {
                    const data = element?.data();
                    data?.photographers?.map(item => {
                        auxArray.push({
                            ...item,
                            rating: item?.rating?.map((element) => element.stars).reduce((accumulator, currentValue) => +accumulator + +currentValue, 0) / item?.rating?.length
                        });
                    })
                }  
            });
            setFavoritesList(auxArray);
        })
    }

    useEffect(() => {
        listenerChangeAlbumsData("favorites", setFavoritesList)
        .then(res => {
            if (res)
                setFavoritesList([]);
        })
        .catch(error => {

        });
      }, [])

    useEffect(() => {
        if (userName && favoritesList?.length === 0) {
            handleGetFavoritesAlbum(userName);
        }
    }, [favoritesList?.length]);


    const handleDeleteFavorites = async (userName, email_photograph) => {
        const newFavList = favoritesList?.filter(element => element?.email !== email_photograph);
        await setAlbumData("favorites", userName, {
            photographers: newFavList
        }, { merge: true }).then(res => {
            if (res) {
                showSuccess();
                setFavoritesList(newFavList);
            }
        })
        .catch(err => {
            showError();
        })
    }

    const showSuccess = () => {
        if (toast)
            toast.current.show({severity:'success', summary: 'Operation succeeded', detail:'Data has been modified', life: 3000});
    };

    const showError = () => {
        if (toast)
            toast.current.show({severity:'error', summary: 'Operation failed', detail:'Data hasn\'t been modified', life: 3000});
    }

    return (
        <div className="grid p-3 m-0">
            <div className="col-12">
                <h2>Favorites List</h2>
                <DataViewTemplate
                data={favoritesList ?? []}
                layout="list"
                renderListItem={(data) => (<div className="col-12">
                    <div className="flex flex-column align-items-center p-3 w-full md:flex-row">
                        {data?.image &&<img className="md:w-11rem w-9 shadow-2 md:my-0 md:mr-5 mr-0 my-5 w-full h-7rem" src={data?.image} alt={data.firstName + " " + data.lastName} />}
                        <div className="text-center md:text-left md:flex-1">
                            {data?.firstName && data?.lastName && <div className="text-2xl font-bold">{data?.firstName + " " + data?.lastName}</div>}
                            {data?.description && <div className="mb-3">{data?.description?.length > 15 ? data?.description?.substring(0, 15) + "..." : data?.description}</div>}
                            {data?.location && <p>{data?.location}</p>}
                            {data?.gallery_photo && <div className="flex flex-column">
                                {
                                    data?.gallery_photo?.map((element, index) => (
                                        <div key={"cat-" + index} className="flex flex-row align-items-center">
                                            <i className="pi pi-tag vertical-align-middle mr-2"></i>
                                            <span className="vertical-align-middle font-semibold">{element?.category}</span>
                                        </div>
                                    ))
                                }
                            </div>}
                        </div>
                        <div className="flex md:flex-column mt-5 justify-content-between align-items-center md:w-auto w-full">
                            <Rating className="mb-2" value={data?.rating ?? 0} readOnly cancel={false}></Rating>
                            <span className="align-self-center text-2xl font-semibold mb-2 md:align-self-end">${data?.hour_rate ?? "0.00"}</span>
                            <Button className="btn-fill mb-2" icon="pi pi-camera" label="View Photographer" onClick={() => history(`/view-photographer/${data?.email}`)}></Button>
                            <Button className="btn-fill mb-2" icon="pi pi-times" label="Remove Photographer" onClick={() => handleDeleteFavorites(userName, data?.email)}></Button>
                        </div>
                    </div>
                </div>)}
                rows={9}
                />
            </div>
            <div className="col-12">
              <Toast ref={toast} />  
            </div>
        </div>
    )
}

export default Favorites;