import React, { createContext } from 'react';
import {
    useLocation,
    useNavigate
  } from "react-router-dom";

export const RoutingContext = createContext(null)

const RoutingProvider = (props)=> {
    const history = useNavigate();
    const location = useLocation();

    return(
        <RoutingContext.Provider value ={{history, location}}>
            {props.children}
        </RoutingContext.Provider> 
    )
}

export default RoutingProvider